import Slider from "react-slick";
import styled from "styled-components";

const StyledSlider = styled(Slider)`
  position: relative;

  .slick-list,
  .slick-slider {
    position: relative;
  }

  .slick-list {
    overflow: hidden;
  }
  .slick-track {
    display: flex;
  }

  .slick-slide {
    height: 100%;
    min-height: 1px;
    > div {
      display: flex;
    }
    img {
      object-fit: cover;
      width: 100%;
    }
  }

  .slick-arrow {
    position: absolute;
    z-index: 1;
    top: 0;
    bottom: 0;
    left: 0;
    width: 15%;
    cursor: pointer;
    text-indent: 100%;
    overflow: hidden;
    padding: 0;
    border: none;
    background: transparent;

    &::before {
      content: "";
      width: 2.5rem;
      height: 2.5rem;
      position: absolute;
      background-color: rgba(255, 255, 255, 0.3);
      left: 0;
      top: 50%;
      transform: translateY(-50%);
      transition: background-color 150ms ease-in-out;
    }
    &::after {
      content: "";
      width: 1rem;
      height: 1rem;
      border: solid #666;
      border-width: 0 0 4px 4px;
      position: absolute;
      left: 1rem;
      top: 50%;
      margin-top: -0.5rem;
      border-radius: 0.125rem;
      transform: rotateZ(45deg);
    }
    &:focus {
      outline: none;
    }
  }

  .slick-arrow:hover::before {
    background-color: rgba(255, 255, 255, 0.6);
  }

  .slick-next {
    cursor: pointer;
    right: 0;
    left: auto;
    &::before {
      left: auto;
      right: 0;
    }
    &::after {
      left: auto;
      right: 1rem;
      transform: rotateZ(-135deg);
    }
  }
  .slick-dots {
    list-style: none;
    padding: 0;
    margin: 0;
    position: absolute;
    left: 50%;
    bottom: 0.5rem;
    transform: translateX(-50%);
    font-size: 0;
    line-height: 0;
    li {
      display: inline-block;
      vertical-align: middle;
    }
    button {
      cursor: pointer;
      border: 5px solid transparent;
      background-color: #eeeeee;
      background-clip: content-box;
      padding: 0;
      border-radius: 100%;
      text-indent: 100%;
      overflow: hidden;
      width: 1.25rem;
      height: 1.25rem;
      transition: background-color 150ms ease-in-out;
    }
    li button:hover {
      background-color: rgba(124, 34, 201, 0.25);
    }
    .slick-active button,
    .slick-active button:hover {
      background-color: ${props => props.theme.colors.purple};
    }
  }
  img,
  a {
    display: block;
  }
`;

export default StyledSlider;
