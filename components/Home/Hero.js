import styled from "styled-components";

const Hero = styled.div`
  display: flex;
  flex-direction: column;
  .quick-links {
    display: flex;
    list-style: none;
    padding: 0;
    margin: 0;
    order: 1;
    flex-wrap: wrap;
    li {
      width: 50%;
    }
    a {
      display: block;
      background: #fff;
      text-align: center;
      font-size: 1.125rem;
      height: 100%;
      padding: 1rem;
      border: solid #eee;
      border-width: 0 0 1px 1px;
    }
    li:nth-child(2n) a {
      border-right-width: 1px;
    }
    .quick-links-text {
      font-size: 0.875rem;
      color: ${props => props.theme.colors.grey};
      font-weight: 400;
    }
    a:hover {
      background-color: ${props => props.theme.colors.purple};
      color: #fff;
      border-color: transparent;
      .quick-links-text {
        color: #fff;
      }
    }
  }

  @media screen and (min-width: 48em) {
    flex-direction: row;
    .quick-links {
      width: 25%;
      order: initial;
      flex-direction: column;
      border-right: 0;
      li {
        flex: 1;
        width: auto;
      }
      a {
        display: flex;
        flex-direction: column;
        justify-content: center;
        border-right: 0;
      }
    }
    .carousel {
      width: 75%;
    }
  }
`;

export default Hero;
