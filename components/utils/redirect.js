import Router from "next/router";

function redirect(url, as) {
  const Redirect = () => {};

  Redirect.getInitialProps = async ({ res }) => {
    if (res) {
      res.writeHead(302, { Location: as || url });
      res.end();
    } else {
      Router.replace(url, as);
    }
    return {};
  };

  return Redirect;
}

export default redirect;
