import { useState } from "react";

export default ({ items, itemsPerPage, initialPage }) => {
  const totalPages = Math.ceil(items.length / itemsPerPage);
  const [currentPage, setPage] = useState(initialPage);
  if (currentPage < 1 && totalPages > 0) setPage(1);
  if (currentPage > totalPages) setPage(totalPages);

  const firstInPage = (currentPage - 1) * itemsPerPage;
  const visibleItems = items.slice(firstInPage, firstInPage + itemsPerPage);

  return {
    visibleItems,
    currentPage,
    totalPages,
    setPage
  };
};
