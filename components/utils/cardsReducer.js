const cardsReducer = (state, action) => {
  console.log("options reduce");
  switch (action.type) {
    case "FILTER": {
      return {
        ...state,
        filterOptions: state.filterOptions.map(opt => ({
          ...opt,
          checked: opt.id === action.id ? 1 : 0
        }))
      };
    }
    case "SORT": {
      return {
        ...state,
        sortOptions: state.sortOptions.map(opt => ({
          ...opt,
          checked: opt.id === action.id ? 1 : 0
        }))
      };
    }
    default:
      return state;
  }
};

export default cardsReducer;
