import { Fragment } from "react";

export default function lineBreaker(str) {
  const split = str.split(/\r\n|\n/g);
  return split.map((text, i) => (
    <Fragment key={i}>
      {text}
      {i < split.length - 1 && <br />}
    </Fragment>
  ));
}
