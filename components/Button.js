import styled, { css } from "styled-components";
import { space, flexbox } from "styled-system";
import Link from "next/link";

const Button = styled.button`
  ${space}
  ${flexbox}
  ${({ block, theme }) => css`
    background: ${theme.colors.purple};
    border: none;
    border-radius: 0.5rem;
    color: #fff;
    padding: 0.875rem;
    font-size: 0.875rem;
    line-height: 1.428571429;
    display: ${block ? "block" : "inline-block"};
    cursor: pointer;
    font-weight: 400;
    min-width: 11rem;

    ${theme.mediaQueries.small} {
      font-size: 1rem;
      line-height: 1.375;
    }
  `}
`;

export default ({ href = "#", type = "button", children, ...props }) => {
  const isExternal = href.indexOf("http") >= 0;
  return isExternal ? (
    <Button href={href} as="a" {...props}>
      {children}
    </Button>
  ) : (
    <Link href={href} passHref>
      <Button as={href && "a"} type={href ? null : type} {...props}>
        {children}
      </Button>
    </Link>
  );
};
