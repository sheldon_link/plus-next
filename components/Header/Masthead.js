import Link from "next/link";
import styled, { css } from "styled-components";
import { useUserState } from "../UserContext";

const Masthead = styled.div`
  outline: 1px solid blue;
  background: #fff;

  .logo {
    height: 2.25rem;
    min-width: 4.0625rem;
    display: block;
  }
  .header-inner {
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 3.5rem;
    padding: 0.5rem;
  }
  .logo-link {
    margin: 0 auto 0 0;
  }
  .meta {
    display: flex;
    list-style: none;
    padding: 0;
    margin: 0;

    li + li {
      margin-left: 1rem;
    }

    a {
      height: 2.5rem;
      display: flex;
      align-items: center;
      justify-content: center;
      white-space: nowrap;
      font-size: 0.875rem;
      color: #666;
    }
    span {
      padding: 0 0.5rem;
    }
    .icon + span {
      padding-left: 0;
    }
  }
  .toggle-nav {
    height: 2.5rem;
    width: 2.5rem;
    padding: 0;
    border: none;
    background: transparent;
    cursor: pointer;
    position: relative;
    z-index: 10;
    margin-right: 0.625rem;
    &:hover {
      background: #ccc;
    }
  }
  .toggle-nav i {
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -0.5625rem;
    margin-top: -0.0625rem;
    background: red;
    width: 1.125rem;
    height: 0.125rem;
    border-radius: 0.625rem;
    transition: transform 200ms ease-in-out;
    transform-origin: center;

    &:nth-child(1) {
      transform: translateY(-0.3125rem);
    }
    &:nth-child(4) {
      transform: translateY(0.3125rem);
    }
  }

  .toggle-nav.is-open i {
    &:nth-child(1),
    &:nth-child(4) {
      transform: translateY(0) scaleX(0);
    }

    &:nth-child(2) {
      transform: rotate(45deg) scaleX(1.15);
    }
    &:nth-child(3) {
      transform: rotate(-45deg) scaleX(1.15);
    }
  }

  ${({ theme }) => css`
    ${theme.mediaQueries.small} {
      .toggle-nav {
        display: none;
      }
      .header-inner {
        height: 5rem;
      }
      .logo {
        height: 3.75rem;
      }
    }
  `}
`;

function NavToggle() {
  const { navIsOpen, toggleNav } = useUserState();
  return (
    <button
      type="button"
      onClick={toggleNav}
      className={`toggle-nav ${navIsOpen ? "is-open" : ""}`}
      aria-label="Open main navigation"
    >
      <i />
      <i />
      <i />
      <i />
    </button>
  );
}

export default () => {
  return (
    <Masthead>
      <div className="contain">
        <div className="header-inner">
          <NavToggle />
          <Link href="/">
            <a className="logo-link">
              <img className="logo" src="/static/img/common/plus-logo@3x.png" />
            </a>
          </Link>

          <ul className="meta">
            <li>
              <Link href="/login">
                <a>
                  <div className="icon">
                    <svg
                      className="icon-svg"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M9.895 2.418a5.5 5.5 0 1 1 4.207 10.164A5.5 5.5 0 0 1 9.895 2.418zm2.276 1.586a3.5 3.5 0 1 0-.345 6.991 3.5 3.5 0 0 0 .345-6.99zM6.002 14.45C7.997 13.65 10.373 13.25 12 13.25c1.627 0 4.003.4 5.998 1.2.998.4 1.964.924 2.696 1.6.74.681 1.306 1.584 1.306 2.7V22H2v-3.25c0-1.116.567-2.019 1.306-2.7.732-.676 1.698-1.2 2.696-1.6zm-1.34 3.07c-.48.443-.662.85-.662 1.231V20h16v-1.25c0-.38-.183-.788-.662-1.23-.486-.45-1.207-.863-2.084-1.214C15.5 15.603 13.377 15.25 12 15.25c-1.377 0-3.5.353-5.254 1.056-.877.351-1.598.765-2.084 1.213z"
                        fill="#666"
                      />
                    </svg>
                  </div>
                  <span className="visually-hidden-mobile">Login</span>
                </a>
              </Link>
            </li>
            <li>
              <a href="#">
                <div className="icon">
                  <svg className="icon-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M7 18c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm10 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-8.9-5h7.4c.7 0 1.4-.4 1.7-1l3.4-6.1c.3-.5.1-1.1-.4-1.4s-1.1-.1-1.4.4L15.6 11h-7l-4-8.4c-.2-.4-.6-.6-1-.6H2c-.5 0-1 .4-1 1s.5 1 1 1h1l3.6 7.6L5.2 14c-.7 1.3.2 3 1.8 3h11c.5 0 1-.5 1-1s-.5-1-1-1H7l1.1-2z" />
                    <path d="M5 6.4h13.2v1.9H5z" />
                  </svg>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="icon">
                  <svg className="icon-svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M16.326 14.56h-.85l-.318-.318a6.932 6.932 0 0 0 1.593-5.596c-.53-2.956-2.974-5.279-5.948-5.596-4.46-.528-8.285 3.168-7.754 7.708.32 2.957 2.762 5.385 5.63 5.913 2.124.317 4.142-.317 5.63-1.584l.318.317v.845l4.461 4.434a1.158 1.158 0 0 0 1.593 0 1.143 1.143 0 0 0 0-1.584l-4.355-4.54zm-6.372 0c-2.656 0-4.78-2.113-4.78-4.752 0-2.64 2.124-4.752 4.78-4.752 2.655 0 4.78 2.112 4.78 4.752 0 2.64-2.125 4.751-4.78 4.751z" />
                  </svg>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </Masthead>
  );
};
