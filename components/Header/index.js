import styled from "styled-components";

import Masthead from "./Masthead";
import Nav from "./Nav";

const Header = styled.header`
  box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.05);
  position: relative;
  z-index: 8;
`;

export default () => (
  <Header>
    <Masthead />
    <Nav />
  </Header>
);
