import { memo } from "react";
import styled, { css } from "styled-components";
import Link from "../utils/ActiveLink";

import { useUserState } from "../UserContext";

const Nav = styled.nav`
  ${({ theme }) => css`
    position: fixed;
    top: 0;
    bottom: 0;
    width: 70%;
    background: #fff;
    padding: 3.5rem 2rem 2rem;
    transform: translateX(-100%);
    transition: transform 200ms ease-in-out;
    z-index: 9;
    &.is-open {
      transform: translateX(0);
    }
    ul {
      font-size: 1.375rem;
      list-style: none;
      margin: 0;
      padding: 0 ${theme.space.sm};
    }
    a {
      display: block;
      color: ${theme.colors.black};
      padding: 0.25rem 0;
      font-weight: 800;
      position: relative;
    }
    a:hover {
      color: ${theme.colors.purple};
    }
    .is-active {
      color: ${theme.colors.purple};
    }

    ${theme.mediaQueries.mobile} {
      .has-subnav > a::after {
        content: "";
        position: absolute;
        width: 0;
        height: 0;
        border-style: solid;
        border-color: ${theme.colors.black} transparent transparent;
        border-width: 6px 6px 0;
        right: 0;
        top: 50%;
        margin-top: -4px;
      }
      .has-subnav:hover a::after {
        border-top-color: ${theme.colors.purple};
      }
      .has-subnav.is-active a::after {
        transform: rotateZ(180deg);
      }
      .has-subnav.is-active ${SubNav} {
        display: block;
      }
    }

    ${theme.mediaQueries.small} {
      padding: 0;
      width: auto;
      position: static;
      transform: none;

      ul {
        display: flex;
        font-size: 1rem;
        li {
          flex: 1 1 auto;
          position: relative;
        }
      }
      a {
        padding: 1rem 0.5rem;
        text-align: center;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
      }

      .has-subnav:hover ${SubNav} {
        display: block;
      }
      .has-subnav:last-child ${SubNav} {
        right: 0;
      }
    }
  `}
`;

const SubNav = styled.div`
  display: none;

  ul {
    margin: 0;
    padding: 0 0 0.75rem;
    flex-direction: column;
    background: #fff;
  }
  a {
    justify-content: flex-start;
    text-align: left;
    padding: 0.25rem 0;
    font-size: 14px;
    font-weight: 500;
  }

  ${({ theme }) => theme.mediaQueries.small} {
    display: none;
    position: absolute;
    min-width: 100%;
    ul {
      border-radius: 0 0 0.5rem 0.5rem;
      box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.05);
    }
    a {
      padding-left: 1.5rem;
      padding-right: 1.5rem;
      white-space: nowrap;
    }
  }
`;

const Overlay = styled.div`
  position: fixed;
  background-color: rgba(0, 0, 0, 0.2);
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  opacity: 0;
  visibility: hidden;
  transition: opacity 200ms ease-in-out, visibility 0s 200ms;
  z-index: 8;

  &.is-active {
    opacity: 1;
    visibility: visible;
    transition: opacity 200ms ease-in-out;
  }

  ${({ theme }) => css`
    ${theme.mediaQueries.small} {
      display: none;
    }
  `}
`;

function handleSubnavLink(e) {
  if (window.innerWidth < 640) {
    e.preventDefault();
    e.target.parentElement.classList.toggle("is-active");
  }
}

const NavContent = memo(() => (
  <div className="contain">
    <ul>
      <li>
        <a href="inner">Update Profile</a>
      </li>
      <li>
        <a href="/registration">Register Card</a>
      </li>
      <li className="has-subnav">
        <Link href="/apply/cards">
          <a onClick={handleSubnavLink}>Be a Plus! Member</a>
        </Link>
        <SubNav>
          <ul>
            <li>
              <Link href="/apply/cards">
                <a>Apply for a Plus! Card</a>
              </Link>
            </li>
            <li>
              <Link href="/mobile-app">
                <a>Apply for free on Plus! App</a>
              </Link>
            </li>
            <li>
              <a href="/member/plus-benefits">Member Benefits</a>
            </li>
          </ul>
        </SubNav>
      </li>
      <li>
        <Link href="/all-partners">
          <a>Our Partners</a>
        </Link>
      </li>
      <li>
        <Link href="/discover/promotions">
          <a>Promotions</a>
        </Link>
      </li>
      <li>
        <a href="/reward/all">Rewards</a>
      </li>
      <li className="has-subnav">
        <a onClick={handleSubnavLink} href="/exchange">
          Plus! Exchange
        </a>
        <SubNav>
          <ul>
            <li>
              <a href="/exchange">About</a>
            </li>
            <li>
              <a href="/exchange/portfolio">My Portfolio</a>
            </li>
          </ul>
        </SubNav>
      </li>
      <li className="has-subnav">
        <a onClick={handleSubnavLink} href="/apply/banking">
          Other Services
        </a>
        <SubNav>
          <ul>
            <li>
              <a href="/apply/banking">Apply for Bank Accounts</a>
            </li>
            <li>
              <a href="/apply/insurance">Apply for Insurance</a>
            </li>
          </ul>
        </SubNav>
      </li>
    </ul>
  </div>
));

export default () => {
  const { navIsOpen, toggleNav } = useUserState();

  return (
    <>
      <Overlay onClick={() => toggleNav(false)} className={`${navIsOpen ? "is-active" : ""}`} />
      <Nav className={navIsOpen ? "is-open" : ""}>
        <NavContent />
      </Nav>
    </>
  );
};
