import styled, { css } from "styled-components";

const StyledOptionsBar = styled.div`
  background: #fff;
  display: flex;
  align-items: start;
  padding: 1rem;
  border-radius: 8px;
  margin-bottom: 1rem;
  font-size: 0.875rem;
  border: 1px solid ${props => props.theme.colors.greyLight};
`;

const Field = styled.div`
  ${props => css`
    display: flex;
    align-items: center;
    margin-right: 1rem;
    white-space: nowrap;
    flex-basis: ${props.categoryFilter ? "150px" : "0"};

    label {
      display: none;
      font-size: 1rem;
      margin-right: 0.5rem;
    }

    &:last-child {
      margin-left: auto;
      margin-right: 0;
    }
    ${props.theme.mediaQueries.small} {
      label {
        display: inline;
      }
      flex-basis: ${props.categoryFilter ? "260px" : "0"};
    }
  `}
`;

const PaginationButton = styled.button`
  ${props => css`
    min-width: 0;
    padding: 9px;
    color: ${props.theme.colors.purple};
    border: 1px solid ${props.theme.colors.purple};
    border-radius: 6px 0 0 6px;
    position: relative;
    z-index: 1;
    cursor: pointer;
    background: #fff;

    ${props.right &&
      css`
        border-radius: 0 6px 6px 0;
        margin-left: -1px;
      `}

    &::before {
      content: "";
      display: block;
      border-style: solid;
      border-width: 2px 0 0 2px;
      border-color: ${props.theme.colors.purple};
      width: 10px;
      height: 10px;
      transform: rotateZ(-45deg);
      position: relative;
      left: 2px;

      ${props.right &&
        css`
          transform: rotateZ(135deg);
          left: -2px;
        `}
    }

    &:hover {
      background-color: ${props.theme.colors.purple};
      &::before {
        border-color: #fff;
      }
    }

    &:disabled {
      border-color: #ccc;
      fill: #ccc;
      background: transparent;
      cursor: not-allowed;
      z-index: 0;
      &::before {
        border-color: #ccc;
      }
    }

    &:focus {
      outline: none;
    }

    ${props.theme.mediaQueries.small} {
      border-radius: 6px;
    }
  `}
`;

const PaginationLabel = styled.div`
  display: none;
  margin: 0 0.5rem;
  ${props => css`
    ${props.theme.mediaQueries.small} {
      display: block;
    }
  `}
`;

export { StyledOptionsBar, Field, PaginationButton, PaginationLabel };
