import Dropdown from "../Dropdown";
import { StyledOptionsBar, Field, PaginationButton, PaginationLabel } from "./OptionsBarUI";

export default ({ options, dispatchAction, pageData, setCurrentPage }) => {
  function handleFilterChange(id) {
    dispatchAction({ type: "FILTER", id });
    setCurrentPage(1);
  }

  function handleSortChange(id) {
    dispatchAction({ type: "SORT", id });
    setCurrentPage(1);
  }

  return (
    <StyledOptionsBar>
      <Field categoryFilter>
        <label>Category:</label>
        <Dropdown handleChange={handleFilterChange} data={options.filterOptions} />
      </Field>
      <Field>
        <label>Sort:</label>
        <Dropdown handleChange={handleSortChange} data={options.sortOptions} />
      </Field>
      <Field>
        <PaginationButton
          disabled={pageData.currentPage <= 1}
          type="button"
          onClick={() => setCurrentPage(x => Math.max(x - 1, 1))}
        />
        <PaginationLabel>
          {pageData.currentPage} of {pageData.totalPages}
        </PaginationLabel>
        <PaginationButton
          disabled={pageData.currentPage === pageData.totalPages}
          right
          type="button"
          onClick={() => setCurrentPage(x => Math.min(x + 1, pageData.totalPages))}
        />
      </Field>
    </StyledOptionsBar>
  );
};
