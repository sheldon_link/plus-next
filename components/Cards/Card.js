import styled from "styled-components";
import Link from "next/link";

const StyledCard = styled.div`
  background: #fff;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;

  .logo {
    max-height: 100px;
    height: 100px;
    margin: 1rem auto;
  }

  .banner {
    padding-bottom: ${({ type }) => (type === "promotion" ? "51.9685%" : "65.3333333%")};
    background-size: cover;
    background-position: center;
    align-self: stretch;
  }

  .category {
    background: #7c22c9;
    color: #fff;
    padding: 0.25rem 1rem;
    margin: 0 auto 1rem;
  }
  h3 {
    padding: 0 1rem;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    overflow: hidden;
    max-width: 100%;
  }

  .promo-period {
    font-weight: 600;
    color: #666;
    margin-bottom: 1rem;
  }
`;

const Card = ({
  type,
  name,
  logo,
  banner,
  newPartner,
  category,
  startDate,
  endDate,
  permalink
}) => {
  return (
    <Link href="">
      <a>
        <StyledCard>
          {/* new tag */}
          <div className="banner" style={{ backgroundImage: `url(${banner})` }} />
          <img className="logo" alt={name} src={logo} />
          {category && <div className="category">{category}</div>}
          <h3>{name}</h3>
          {startDate && endDate && (
            <div className="promo-period">
              {startDate} - {endDate}
            </div>
          )}
        </StyledCard>
      </a>
    </Link>
  );
};

export default Card;
