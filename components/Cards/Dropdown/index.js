import { useRef, useState, useEffect } from "react";

import { StyledDropdown, Field, Options, Option } from "./DropdownUI";

export default ({ handleChange, data = [] }) => {
  const dropdownEl = useRef(null);
  const [active, setActive] = useState(false);
  const toggleDropdown = () => {
    setActive(state => !state.active);
  };

  const activeData = data.find(obj => obj.checked === 1);

  function handleClickOutside(e) {
    if (e.target.parentElement === dropdownEl.current) return;
    setActive(false);
  }

  useEffect(() => {
    window.addEventListener("click", handleClickOutside);
    return function cleanup() {
      window.removeEventListener("click", handleClickOutside);
    };
  });

  return (
    <StyledDropdown ref={dropdownEl} onClick={toggleDropdown}>
      <Field>{activeData.name}</Field>
      <Options active={active}>
        {data.map(({ name, id }) => (
          <Option active={id === activeData.id} key={id} onClick={() => handleChange(id)}>
            {name}
          </Option>
        ))}
      </Options>
    </StyledDropdown>
  );
};
