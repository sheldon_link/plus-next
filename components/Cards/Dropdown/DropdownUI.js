import styled from "styled-components";

const StyledDropdown = styled.div`
  cursor: pointer;
  text-align: left;
  white-space: nowrap;
  max-width: 100%;
  flex: 1;
  min-width: 90px;
  position: relative;
`;

const Field = styled.div`
  border: 1px solid #a4a4a4;
  padding: 0.25rem 2rem 0.25rem 1rem;
  border-radius: 6px;
  overflow: hidden;
  text-overflow: ellipsis;
  position: relative;

  &::after {
    content: "";
    position: absolute;
    top: 50%;
    right: 0.75rem;
    border-style: solid;
    border-width: 5px 5px 0;
    border-color: #7c22c9 transparent transparent;
    transform: translateY(-50%);
  }
`;

const Options = styled.div`
  display: ${props => (props.active ? "block" : "none")};
  position: absolute;
  background: #fff;
  z-index: 9;
  border-radius: 4px;
  box-shadow: 0 2px 6px 0 rgba(102, 102, 102, 0.5);
  width: 100%;
`;

const Option = styled.div`
  padding: 0.25rem 1rem;
  overflow: hidden;
  text-overflow: ellipsis;

  &:hover {
    background: #efe8f8;
  }
`;

export { StyledDropdown, Field, Options, Option };
