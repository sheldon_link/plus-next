import { useReducer } from "react";
import { parse, isBefore, isAfter } from "date-fns";

import { Flex, Box } from "../Grid";
import Card from "./Card";
import CardGrid from "./CardGrid";
import usePagination from "../utils/usePagination";
import cardsReducer from "../utils/cardsReducer";
import OptionsBar from "./OptionsBar";

function sortFunctions(sortBy) {
  if (sortBy === "name-asc") {
    return (a, b) => {
      if (a.merchantName.toUpperCase() < b.merchantName.toUpperCase()) {
        return -1;
      }
      if (a.merchantName.toUpperCase() > b.merchantName.toUpperCase()) {
        return 1;
      }
      return 0;
    };
  }
  if (sortBy === "name-desc") {
    return (a, b) => {
      if (a.merchantName.toUpperCase() > b.merchantName.toUpperCase()) {
        return -1;
      }
      if (a.merchantName.toUpperCase() < b.merchantName.toUpperCase()) {
        return 1;
      }
      return 0;
    };
  }
  if (sortBy === "date-newest") {
    return (a, b) => {
      const firstDate = parse(a.publishDate, "dd/MM/yyyy HH:mm:ss", new Date());
      const secondDate = parse(b.publishDate, "dd/MM/yyyy HH:mm:ss", new Date());
      if (isBefore(firstDate, secondDate)) {
        return 1;
      }
      if (isAfter(firstDate, secondDate)) {
        return -1;
      }
      return 0;
    };
  }
  return null;
}

function CardList({ cards, filterOptions }) {
  const sortOptions = [
    {
      id: "name-asc",
      name: "A to Z",
      checked: 1
    },
    {
      id: "name-desc",
      name: "Z to A",
      checked: 0
    },
    {
      id: "date-newest",
      name: "Newest",
      checked: 0
    }
  ];

  const initialOptions = {
    filterOptions: [
      {
        id: -1,
        name: "All",
        checked: 1
      },
      ...filterOptions
    ],
    sortOptions
  };

  const [options, dispatchAction] = useReducer(cardsReducer, initialOptions);
  const activeFilter = options.filterOptions.find(obj => obj.checked === 1);
  const activeSort = options.sortOptions.find(obj => obj.checked === 1);
  let updatedCards = cards;

  if (activeSort) {
    updatedCards = updatedCards.sort(sortFunctions(activeSort.id));
  }
  if (activeFilter && activeFilter.id > -1) {
    updatedCards = cards.filter(card => card.categoryIds.some(cat => cat.id === activeFilter.id));
  }

  const { visibleItems, currentPage, totalPages, setPage } = usePagination({
    items: updatedCards,
    itemsPerPage: 12,
    initialPage: 1
  });

  return (
    <>
      <OptionsBar
        options={options}
        dispatchAction={dispatchAction}
        pageData={{ currentPage, totalPages }}
        setCurrentPage={setPage}
      />
      <CardGrid>
        {visibleItems.map(p => {
          const { merchantName, headerURL, merchantBigLogoURL, id, permalink } = p;
          return (
            <Card
              key={id}
              permalink={permalink}
              banner={headerURL}
              name={merchantName}
              logo={merchantBigLogoURL}
            />
          );
        })}
      </CardGrid>
      <OptionsBar
        options={options}
        dispatchAction={dispatchAction}
        pageData={{ currentPage, totalPages }}
        setCurrentPage={setPage}
      />
    </>
  );
}

export default CardList;
