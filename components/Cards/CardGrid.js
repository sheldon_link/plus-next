import React from "react";
import { css } from "styled-components";
import { Flex, Box } from "../Grid";

export default ({ rail, children }) => {
  return (
    <Flex
      css={css`
        overflow-x: ${rail ? "auto" : "initial"};
        flex-wrap: ${rail ? "nowrap" : "wrap"};
      `}
      mx="-5px"
    >
      {React.Children.map(children, Child => (
        <Box flexShrink="0" flexBasis={["50%", "33%", "25%"]} px="5px" mb="10px">
          {Child}
        </Box>
      ))}
    </Flex>
  );
};
