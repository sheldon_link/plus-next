import { ThemeProvider } from "styled-components";
import GlobalStyles, { theme } from "./GlobalStyles";

import { UserContextProvider } from "./UserContext";
import Header from "./Header";
import Footer from "./Footer";

const Layout = ({ children }) => (
  <ThemeProvider theme={theme}>
    <UserContextProvider>
      <GlobalStyles />
      <Header />
      {children}
      <Footer />
    </UserContextProvider>
  </ThemeProvider>
);

export default Layout;
