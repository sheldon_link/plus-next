import { useState, useContext, createContext } from "react";

const UserContext = createContext();

function UserContextProvider({ children }) {
  const [navIsOpen, setNavIsOpen] = useState(false);

  return (
    <UserContext.Provider value={{ navIsOpen, setNavIsOpen }}>{children}</UserContext.Provider>
  );
}

function useUserState() {
  const { navIsOpen, setNavIsOpen } = useContext(UserContext);
  const toggleNav = isOpen => {
    if (typeof isOpen === "boolean") {
      setNavIsOpen(isOpen);
    } else {
      setNavIsOpen(!navIsOpen);
    }
  };
  return { navIsOpen, toggleNav };
}

export { UserContextProvider, useUserState };
