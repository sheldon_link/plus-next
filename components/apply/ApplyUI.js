import styled from "styled-components";
import { theme } from "../GlobalStyles";

const OuterRail = styled.div`
  ${theme.mediaQueries.small} {
    transform: translateX(50%);
  }
`;
const Rail = styled.div`
  display: flex;
  align-items: flex-end;
  transition: transform 400ms ease-in-out;
  transform: ${props => `translate3d(-${props.offset * 100}%, 0, 0);`};

  ${theme.mediaQueries.small} {
    height: 250px;
    margin-left: -194px;
    transform: ${props => `translate3d(-${props.offset * 195}px, 0, 0);`};
  }
`;

const Card = styled.div`
  padding: 0 0px;
  transition: width 400ms ease-in-out;
  flex: 1 0 100%;
  cursor: pointer;

  img {
    display: block;
    min-width: 183px;
    width: 50vw;
    margin: 0 auto;
    transition: transform 400ms ease-in-out;
    transform-origin: center 93%;
    user-drag: none;
  }

  ${theme.mediaQueries.small} {
    width: ${({ active }) => (active ? "388px" : "195px")};
    flex: 0 0 auto;
    img {
      width: 183px;
      transform: ${({ active }) => (active ? "scale3d(2, 2, 1)" : "none")};
      will-change: width;
    }
  }
`;

const CardTitle = styled.h2`
  text-align: center;
  margin: 0;
  height: 4rem; /* 64px : magic number of 2 x line-height */
  display: flex;
  align-items: center;
  justify-content: center;
`;

const CaretButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  width: 3rem;
  height: 3rem;
  background: url(/static/img/common/caret-big.svg) no-repeat center;
  background-size: 1.5rem 1.5rem;
  &.left {
    margin-left: -2rem;
  }
  &.right {
    transform: scaleX(-1);
    margin-right: -2rem;
  }
  ${theme.mediaQueries.small} {
    background-size: auto;
  }

  &:disabled {
    opacity: 0.25;
  }
`;

const TextHolder = styled.div`
  margin-bottom: 1rem;
  ul {
    list-style: none;
    padding: 0;
    margin: 0 auto 2.5rem;
    max-width: 24.375rem;
  }

  li {
    display: flex;
    margin-bottom: ${theme.space.sm};
  }

  li::before {
    content: "✓";
    display: inline-block;
    color: ${theme.colors.purpleLight};
    margin-right: ${theme.space.sm};
  }
`;

const Fader = styled.div`
  transition: opacity 150ms ease;
  &.fade-exit-active,
  &.fade-exit-done {
    opacity: 0;
  }
`;

const Option = styled.div`
  height: 100%;
  text-align: center;
  border: 1px solid ${theme.colors.purpleLight};
  border-radius: 0.5rem;
  padding: ${theme.space.lg};
  display: flex;
  flex-direction: column;
  align-items: center;
  h2,
  p {
    margin-top: 0;
    max-width: 39rem;
  }
`;

const Table = styled.table.attrs({ cellSpacing: 0, cellPadding: 0 })`
  outline: 1px solid red;
  min-width: 75rem;
  font-size: 0.875rem;
  th,
  td {
    width: 196px;
    text-align: center;
    padding: 0 0.625rem;
  }
  th:first-child,
  td:first-child {
    width: auto;
    padding-left: 0;
    text-align: left;
  }

  td {
    padding-top: 1.4375rem;
    padding-bottom: 1.4375rem;
  }
  .symbol {
    font-weight: 800;
    font-size: 1.25rem;
  }

  tbody tr td {
    border-top: 1px solid ${theme.colors.purpleLight};
  }

  .card-titles th {
    padding-bottom: 1rem;
  }

  ${theme.mediaQueries.small} {
    font-size: 1rem;
  }
`;

export { OuterRail, Rail, Card, CardTitle, CaretButton, TextHolder, Fader, Option, Table };
