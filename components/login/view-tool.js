import * as views from "./views";

export default ({ state, dispatch }) => (
  <div>
    {Object.values(views).map(view => (
      <button
        disabled={state.view === view}
        type="button"
        onClick={() => dispatch({ type: "CHANGE_VIEW", value: view })}
      >
        {view.name}
      </button>
    ))}
  </div>
);
