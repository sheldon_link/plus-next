import { useState } from "react";
import useForm from "react-hook-form";

function fetchAddress(postalCode) {
  console.log(`%c fetching postal code: ${postalCode}`, "color: green");
  const fetchHeaders = {
    "Content-Type": "application/json"
  };
  return fetch("https://mobility-uat.ntuclink.cloud/tribeservice/v2/getAddress", {
    method: "POST",
    headers: fetchHeaders,
    body: JSON.stringify({ body: { postalCode }, metaData: { client: "website" } })
  });
}

const CardApply = ({ state, dispatch }) => {
  const { register, handleSubmit, setValue, setError, clearError, getValues, errors } = useForm();
  const [addressText, setAddressText] = useState(null);
  register({ name: "address" }, { required: true });

  const onSubmit = data => {
    console.log("submitting this info:");
    console.log(data);
  };

  async function handlePostalCodeChange(e) {
    const postalCode = e.target.value;
    if (postalCode.length < 6) {
      setAddressText(null);
      return;
    }
    const res = await fetchAddress(postalCode);
    const json = await res.json();
    if (json.code === "0000") {
      const {
        response: {
          postalCodeData,
          chatBootresponse: {
            chatServerMsg: [{ displayMessage }]
          }
        }
      } = json;
      clearError("postalCode");
      setValue("address", postalCodeData);
      setAddressText(displayMessage.replace("Your address is", ""));
    } else {
      setError("postalCode", "validation", "invalid postal code");
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {state.viewHistory.length ? (
        <button type="button" onClick={() => dispatch({ type: "BACK" })}>
          back
        </button>
      ) : null}
      <h1>
        We will mail you a free Plus! card so you can fully enjoy a world of delights and rewards.
        Please enter your details below.
      </h1>

      <div>
        <input
          placeholder="Given Name"
          name="fname"
          type="text"
          ref={register({
            required: "Please enter your given name",
            pattern: {
              value: /^(?![0-9 ]*$)[a-zA-Z0-9-’' ]+$/,
              message: "Please check your given name, it does not seem right."
            }
          })}
        />
        {errors.fname && errors.fname.message}
      </div>
      <div>
        <input
          placeholder="Surname"
          name="lname"
          type="text"
          ref={register({
            require: "needed"
          })}
        />
        {errors.lname && errors.lname.message}
      </div>
      <div>
        <input placeholder="DOB" name="dob" type="text" />
      </div>
      <div>
        <input
          minLength="6"
          maxLength="6"
          placeholder="Postal Code"
          name="postalCode"
          type="text"
          onChange={handlePostalCodeChange}
          ref={register({
            required: "Please enter a postal code"
          })}
        />
        {errors.postalCode && errors.postalCode.message}
        <br />
        {addressText}
      </div>
      <button onClick={() => console.log(getValues())}>get</button>
      <button type="submit">Submit</button>
    </form>
  );
};

export default CardApply;
