import MobileLanding from "./MobileLanding";
import MobileVerifyOTP from "./MobileVerifyOTP";
import MobileUpdate from "./MobileUpdate";
import EmailPrompt from "./EmailPrompt";
import CardPrompt from "./CardPrompt";
import CardRegister from "./CardRegister";
import CardApply from "./CardApply";
import CustomerService from "./CustomerService";
import SecurityDOB from "./SecurityDOB";
import SecurityMobile from "./SecurityMobile";

export {
  MobileLanding,
  MobileVerifyOTP,
  MobileUpdate,
  EmailPrompt,
  CardPrompt,
  CardRegister,
  CardApply,
  CustomerService,
  SecurityDOB,
  SecurityMobile
};
