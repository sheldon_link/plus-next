import useForm from "react-hook-form";

const EmailPrompt = ({ dispatch }) => {
  const { register, handleSubmit, errors } = useForm();

  function onSubmit(values) {
    console.log("submitting email promp");
    dispatch({
      type: "UPDATE_PAYLOAD",
      value: values,
      remove: "resendOTP"
    });
    dispatch({
      type: "PUSH_VIEW_HISTORY",
      value: EmailPrompt
    });
    dispatch({
      type: "UPDATE_ENDPOINT",
      value: "/v1/verifyOnboardEmail"
    });
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1>To help us verify your account, we'll need your email address.</h1>
      <input
        name="email"
        type="email"
        ref={register({
          required: "Please enter your email address to continue",
          pattern: {
            value: /^[\w-\+]+(\.[\w]+)*@[\w-]+(\.[\w]+)*(\.[a-z]{2,70})$/,
            message: "Please enter a valid email address to continue"
          }
        })}
      />
      {errors.email && errors.email.message}
      <button type="submit">Submit</button>
    </form>
  );
};

export default EmailPrompt;
