import useForm from "react-hook-form";

const MobileLanding = ({ dispatch }) => {
  const { register, handleSubmit, errors } = useForm();

  function onSubmit(values) {
    dispatch({
      type: "UPDATE_PAYLOAD",
      value: { ...values, resendOTP: 0 }
    });
    dispatch({
      type: "PUSH_VIEW_HISTORY",
      value: MobileLanding
    });
    dispatch({
      type: "UPDATE_ENDPOINT",
      value: "/v2/getOTPWithMobile"
    });
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1>Start your journey with us!</h1>
      <input
        name="mobileNumber"
        type="text"
        ref={register({
          required: "Please enter your mobile number"
          // pattern: {
          //   value: /^[8,9]{1}[0-9]{7}$/,
          //   message: "Please enter a valid mobile number. e.g. 91234567"
          // }
        })}
      />
      {errors.mobileNumber && errors.mobileNumber.message}

      <button type="submit">Submit</button>
    </form>
  );
};

export default MobileLanding;
