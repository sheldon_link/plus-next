import useForm from "react-hook-form";
import CardApply from "./CardApply";
import CustomerService from "./CustomerService";

const renderAlert = alert => {
  if (alert) {
    return (
      <div>
        {alert.type} : {alert.message}
      </div>
    );
  }
  return null;
};

const CardPrompt = ({ state, dispatch }) => {
  const { register, handleSubmit, errors } = useForm();
  function onSubmit(values) {
    dispatch({
      type: "UPDATE_PAYLOAD",
      value: values,
      remove: "resendOTP"
    });
    dispatch({
      type: "UPDATE_ENDPOINT",
      value: "/v1/enrollViaCard"
    });
  }
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {renderAlert(state.alert)}
      {state.viewHistory.length ? (
        <button type="button" onClick={() => dispatch({ type: "BACK" })}>
          back
        </button>
      ) : null}
      <h1>To help us identify you better, please enter your 16-digit card number.</h1>
      <input
        name="cardNo"
        type="text"
        maxLength="19"
        ref={register({
          required: "Please enter a card number to continue",
          pattern: {
            value: /^[0-9]{16}$/,
            message: "Please enter a valid card number to continue"
          }
        })}
      />
      <button type="submit">Submit</button>
      <button type="button" onClick={() => dispatch({ type: "CHANGE_VIEW", value: CardApply })}>
        Don't have Card
      </button>
      <button
        type="button"
        onClick={() => dispatch({ type: "CHANGE_VIEW", value: CustomerService })}
      >
        Lost Card
      </button>
    </form>
  );
};

export default CardPrompt;
