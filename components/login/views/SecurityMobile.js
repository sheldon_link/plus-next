import useForm from "react-hook-form";

const SecurityMobile = ({ state, dispatch }) => {
  const { register, handleSubmit, errors } = useForm();

  function onSubmit(values) {
    dispatch({
      type: "UPDATE_PAYLOAD",
      value: values
    });
    dispatch({
      type: "UPDATE_ENDPOINT",
      value: "/v1/verifyOnboardSecurity"
    });
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1>
        Your contact details have changed. Please update it by entering your previous mobile number.
      </h1>
      {state.tip ? `Tip: ${state.tip}` : null}
      <input
        name="securityAnswer"
        type="text"
        ref={register({
          required: "Please enter your mobile number (QA mode)"
        })}
      />
      {errors.email && errors.email.message}
      <button type="submit">Submit</button>
    </form>
  );
};

export default SecurityMobile;
