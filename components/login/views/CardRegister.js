import useForm from "react-hook-form";

const CardRegister = ({ state, dispatch }) => {
  const { register, handleSubmit, setValue, setError, clearError, getValues, errors } = useForm();

  function onSubmit(values) {}

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {state.viewHistory.length ? (
        <button type="button" onClick={() => dispatch({ type: "BACK" })}>
          back
        </button>
      ) : null}

      <h1>Let's get you registered. Tell us more about yourself</h1>
      <div>
        <input
          placeholder="Given Name"
          name="fname"
          type="text"
          ref={register({
            required: "Please enter your given name",
            pattern: {
              value: /^(?![0-9 ]*$)[a-zA-Z0-9-’' ]+$/,
              message: "Please check your given name, it does not seem right."
            }
          })}
        />
        {errors.fname && errors.fname.message}
      </div>
      <div>
        <input
          placeholder="Surname"
          name="lname"
          type="text"
          ref={register({
            require: "needed"
          })}
        />
        {errors.lname && errors.lname.message}
      </div>
      <div>
        <input placeholder="DOB" name="dob" type="text" />
      </div>
      <button type="submit">Submit</button>
    </form>
  );
};

export default CardRegister;
