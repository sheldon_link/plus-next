import useForm from "react-hook-form";

const MobileVerifyOTP = ({ state, dispatch }) => {
  const { register, handleSubmit, errors } = useForm();

  const renderAlert = alert => {
    if (alert) {
      return (
        <div>
          {alert.type} : {alert.message}
        </div>
      );
    }
    return null;
  };

  function onSubmit(values) {
    dispatch({
      type: "UPDATE_PAYLOAD",
      value: values
    });
    dispatch({
      type: "UPDATE_ENDPOINT",
      value: "/v3/verifyOtpWithMobile"
    });
  }
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {renderAlert(state.alert)}

      {state.viewHistory.length ? (
        <button type="button" onClick={() => dispatch({ type: "BACK" })}>
          back
        </button>
      ) : null}
      <h1>VerifyOTP(screen 2)</h1>
      <input
        name="otp"
        type="text"
        ref={register({
          required: "Please enter the OTP to continue",
          pattern: {
            value: /^[0-9]{6}$/,
            message: "Please enter the correct OTP to continue"
          }
        })}
      />
      <button type="submit">Submit</button>
    </form>
  );
};

export default MobileVerifyOTP;
