import {
  MobileVerifyOTP,
  MobileUpdate,
  EmailPrompt,
  CardPrompt,
  CardRegister,
  SecurityDOB,
  SecurityMobile
} from "./views";

const Error = () => <div>error</div>;

function viewResolver(json) {
  const { endpointType } = json.response.chatBootresponse.chatServerMsg.pop().nextMessage[0];

  console.log(`endpointType detected: ${endpointType}`);
  switch (endpointType) {
    case "VERIFY_MOB_OTP":
      return MobileVerifyOTP;
    case "VERIFY_EMAIL":
      return EmailPrompt;
    case "VERIFY_EMAIL_OTP":
      return MobileUpdate;
    case "ENTER_CARD_NO":
      return CardPrompt;
    case "SECURITY_DOB":
      return SecurityDOB;
    case "SECURITY_MOBILE":
      return SecurityMobile;
    case "PREISSUED_CARD":
      return CardRegister;
    default:
      return Error;
  }
}

export default viewResolver;
