import styled from "styled-components";
import { layout, space, flexbox } from "styled-system";

const Contain = styled.div`
  max-width: 80rem;
  width: 100%;
  padding-left: 2.5rem;
  padding-right: 2.5rem;
  margin-left: auto;
  margin-right: auto;
`;

const Flex = styled.div`
  ${layout}
  ${flexbox}
  ${space}
  display: flex;
`;

const Box = styled.div`
  ${space}
  ${layout}
  ${flexbox}
`;

const Row = styled(Flex)`
  margin-left: -1rem;
  margin-right: -1rem;
`;

const Col = styled(Box)`
  padding-left: 1rem;
  padding-right: 1rem;
`;

export { Contain, Flex, Box, Row, Col };
