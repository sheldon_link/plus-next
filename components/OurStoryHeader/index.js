import styled from "styled-components";
import Link from "../utils/ActiveLink";

const StyledNav = styled.ul`
  outline: 1px solid red;
  display: flex;
  list-style: none;

  li {
    margin: 0 1rem;
  }

  a.is-active {
    color: red;
  }
`;

export default () => {
  return (
    <>
      <h1>Our Story</h1>
      <StyledNav>
        <li>
          <Link href="/our-story/about-us">
            <a>About Us</a>
          </Link>
        </li>
        <li>
          <Link href="/our-story/vision-mission-values">
            <a>Vision, Mission &amp; Values</a>
          </Link>
        </li>
        <li>
          <Link href="/our-story/milestones-achievements">
            <a>Milestones &amp; Achievements</a>
          </Link>
        </li>
        <li>
          <Link href="/our-story/facts-figures">
            <a>Facts &amp; Figures</a>
          </Link>
        </li>
        <li>
          <Link href="/our-story/our-members">
            <a>Our Members</a>
          </Link>
        </li>
      </StyledNav>
    </>
  );
};
