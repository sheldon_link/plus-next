import styled, { css } from "styled-components";

import FooterNav from "./FooterNav";
import FooterStripe from "./FooterStripe";

const Footer = styled.footer`
  outline: 1px solid red;
  margin-top: auto;

  .footer-links {
    background: #f1f1f1;
  }

  .footer-links ul {
    padding: 1rem 0;
    margin: 0;
    text-transform: uppercase;
    font-size: 0.875rem;
    list-style: none;
  }
  .footer-links li {
    flex: 1;
    text-align: center;
  }
  .footer-links a {
    display: block;
    color: ${({ theme }) => theme.colors.black};
    line-height: 3.5;
    transition: background-color 300ms ease-out;
  }
  .footer-links a:hover {
    background-color: #c4c3c3;
  }

  ${({ theme }) => css`
    ${theme.mediaQueries.small} {
      .toggle-nav {
        display: none;
      }
      .header-inner {
        height: 5rem;
      }
      .logo {
        height: 3.75rem;
      }
      .footer-links ul {
        display: flex;
        padding: 0;
      }
      .footer-links a {
        line-height: 5;
      }
    }
  `}
`;

export default () => (
  <Footer>
    <FooterNav />
    <FooterStripe>an NTUC Social Enterprise</FooterStripe>
    <div className="footer-links">
      <div className="contain">
        <ul>
          <li>
            <a href="https://plus.com.sg">Check Linkpoints</a>
          </li>
          <li>
            <a href="https://plus.com.sg">Contact Us</a>
          </li>
          <li>
            <a href="https://plus.com.sg">Be a Partner</a>
          </li>
        </ul>
      </div>
    </div>
  </Footer>
);
