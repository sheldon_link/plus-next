import Link from "next/link";
import styled, { css } from "styled-components";
import { Contain, Row, Col, Box } from "../Grid";

const Title = styled.h3`
  ${({ theme }) => css`
    margin: 0 0 0.25rem;
    text-transform: uppercase;
    font-size: 1rem;
    color: ${theme.colors.black};
  `}
`;

const Links = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  font-size: 0.875rem;
  line-height: 1.75;

  a {
    color: #666;
    font-weight: 400;
  }
`;

const SocialMediaLinks = styled.div`
  display: flex;
  margin-top: ${({ theme }) => theme.space.sm};
  padding-top: ${({ theme }) => theme.space.xs};
  a {
    width: 1.875rem;
    height: 1.875rem;
    margin-right: ${({ theme }) => theme.space.md};
  }
`;

export default () => (
  <Contain>
    <Row flexDirection={["column", "row"]} pt="md">
      <Col flex="1" mb="md">
        <Title>Member's Corner</Title>
        <Links>
          <li>
            <a href="/registration">Create a Login Account</a>
          </li>
          <li>
            <a href="/apply/cards">Apply for a Card</a>
          </li>
          <li>
            <a href="/member/how-to-earn">Earn LinkPoints</a>
          </li>
          <li>
            <a href="/member/how-to-redeem">Redeem LinkPoints</a>
          </li>
          <li>
            <a href="/ez-link">EZ-Link</a>
          </li>
          <li>
            <a href="/google-pay">Google Pay</a>
          </li>
          <li>
            <a
              data-bypass
              data-plus
              href="https://support.plus.com.sg/hc/en-us/sections/360004609192-Banking-Products"
            >
              Forms & Others
            </a>
          </li>
        </Links>
      </Col>

      <Col flex="1" mb="md" display="flex" flexDirection="column" justifyContent="space-between">
        <Box>
          <Title>Be a Plus! Partner</Title>
          <Links>
            <li>
              <a href="/be-our-partner">Apply to be a Partner</a>
            </li>
          </Links>
        </Box>
        <Box mt="md">
          <Title>Get to Know Us</Title>
          <Links>
            <li>
              <Link href="/our-story/about-us">
                <a>Our Story</a>
              </Link>
            </li>
            <li>
              <a href="/our-team/management-team">Our Team</a>
            </li>
            <li>
              <a data-bypass data-plus href="https://ntuclink.recruiterpal.com/">
                Careers
              </a>
            </li>
            <li>
              <a data-bypass data-plus href="/stories/">
                Plus! Stories
              </a>
            </li>
          </Links>
        </Box>
      </Col>

      <Col flex="1" mb="md">
        <Title>Customer Care</Title>
        <Links>
          <li>
            <a data-bypass data-plus href="/profile/card-replacement">
              Card Replacement
            </a>
          </li>
          <li>
            <a data-bypass data-plus href="/page/bank-notices">
              Bank Notice
            </a>
          </li>
          <li>
            <a
              data-bypass
              data-plus
              data-ignore
              href="https://support.plus.com.sg/hc/en-us/sections/360004609192-Banking-Products"
            >
              Banking Services
            </a>
          </li>
          <li>
            <a data-bypass data-plus target="_blank" href="/pdf/Plus_Member_Guide.pdf">
              Sign Up Guide
            </a>
          </li>
          <li>
            <a data-bypass data-plus href="/legal/terms-conditions">
              Terms and Conditions
            </a>
          </li>
          <li>
            <a data-bypass data-plus href="/legal/privacy-policy">
              Privacy Policy
            </a>
          </li>
        </Links>
      </Col>

      <Col flex="1" mb="md" display="flex" flexDirection="column" justifyContent="space-between">
        <Box>
          <Title>Connect With Us</Title>
          <SocialMediaLinks>
            <a
              target="_blank"
              href="https://instagram.com/mypluslife"
              className="instagram-icon sociaMediaIcon"
              aria-label="Plus! Instagram"
              rel="noreferrer"
              data-ignore
            >
              <img src="/static/img/instagram.svg" />
            </a>
            <a
              target="_blank"
              href="https://www.facebook.com/plus.linkpoints"
              className="facebook-icon sociaMediaIcon"
              aria-label="Plus! Facebook"
              rel="noreferrer noopener"
              data-ignore
            >
              <img src="/static/img/facebook.svg" />
            </a>
          </SocialMediaLinks>
        </Box>
        <Box>
          <small>Join our mailing list for exclusive promotions</small>
        </Box>
      </Col>
    </Row>
  </Contain>
);
