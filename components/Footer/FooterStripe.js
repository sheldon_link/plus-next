import styled from "styled-components";

const FooterStripe = styled.div`
  display: -ms-grid;
  -ms-grid-columns: 1fr minmax(min-content, 1160px) 1fr;
  display: grid;
  grid-template-columns: 1fr minmax(min-content, 1160px) 1fr;
  align-items: center;
  background: #fff;
  padding: 0.6875rem 0 0.75rem;

  &::before {
    content: "";
    background: #ffa300;
    height: 10px;
    margin-right: -13px;
    position: relative;
  }
  .inner {
    -ms-grid-column: 2;
    -ms-grid-column-span: 2;
    display: flex;
    grid-column-start: 2;
    grid-column-end: span 2;
    align-items: center;
  }
  .inner::after {
    content: "";
    background-image: linear-gradient(to left, #1d35df, #7c22c9 42%, #d0008a 74%, #ffa300);
    height: 10px;
    width: 100%;
    display: block;
    position: relative;
  }
  span {
    font-weight: 700;
    font-size: 17px;
    color: #f2a73b;
    white-space: nowrap;
    background: #fff;
    padding: 0 0.5rem 0 20px;
  }
`;

export default ({ children }) => (
  <FooterStripe>
    <div className="inner">
      <span>{children}</span>
    </div>
  </FooterStripe>
);
