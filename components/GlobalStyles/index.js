import { createGlobalStyle } from "styled-components";
import styledNormalize from "styled-normalize";
import fonts from "./_fonts";
import typography from "./_typography";
import theme from "./theme";

const GlobalStyles = createGlobalStyle`
  ${styledNormalize}
  ${fonts}
  ${typography}

  
  *, *::before, *::after {
    box-sizing: border-box;
  }

  body {
    margin: 0;
    padding: 0;
    line-height: 1.5;
  }

  #__next {
    min-height: 100vh;
    display: flex;
    flex-direction: column;
  }

  img {
    max-width: 100%;
    height: auto;
  }

   hr {
    border-width: 1px 0 0;
    border-style: solid;
    border-color: #dadada;
    margin: 2rem 0;
  }

  .contain {
    max-width: 75rem;
    width: 100%;
    margin-left: auto;
    margin-right: auto;
  }

  .icon {
    width: 2.5rem;
    height: 2.5rem;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .icon-svg {
    width: 1.5rem;
    height: 1.5rem;
    fill: #666;
  }
  .text-center {
    text-align: center;
  }
  .visually-hidden {
    clip: rect(0, 0, 0, 0);
    clip-path: inset(50%);
    height: 1px;
    width: 1px;
    margin: -1px;
    padding: 0;
    overflow: hidden;
    position: absolute;
  }

  ${theme.mediaQueries.mobile} {
    .visually-hidden-mobile {
      clip: rect(0, 0, 0, 0);
      clip-path: inset(50%);
      height: 1px;
      width: 1px;
      margin: -1px;
      padding: 0;
      overflow: hidden;
      position: absolute;
    } 
  }

`;

export default GlobalStyles;

export { theme };
