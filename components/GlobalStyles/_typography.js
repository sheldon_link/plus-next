import { css } from "styled-components";

export default ({ theme }) => css`
  body {
    font-family: "Lato", Helvetica, Arial, sans-serif;
    color: ${theme.colors.grey};
    font-weight: 400;
  }

  a {
    color: ${theme.colors.purple};
    font-weight: 700;
    text-decoration: none;
  }

  h1 {
    font-size: 2.25rem; /* 36px */
    font-weight: 800;
    line-height: 1.222222222;
    color: ${theme.colors.black};
  }

  h2 {
    font-size: 1.5rem; /* 24px */
    font-weight: 700;
    line-height: 1.333333333;
    color: ${theme.colors.black};
  }

  .subtitle {
    font-weight: 500;
  }
  small,
  .small {
    font-size: 0.875rem; /* 14px */
    font-weight: 400;
    line-height: 1.428571429;
    color: ${theme.colors.grey};
  }

  ${theme.mediaQueries.small} {
    body {
      font-size: 1.25rem;
    }
    h1 {
      font-size: 2.8125rem; /* 45px */
    }
    h2 {
      font-size: 1.875rem; /* 30px */
    }
    small,
    .small {
      font-size: 1rem;
    }
  }
`;
