import { css } from "styled-components";

export default css`
  /* Lato Regular */
  @font-face {
    font-family: "Lato";
    src: url("/static/fonts/Lato/LatoLatin-Regular.eot");
    src: url("/static/fonts/Lato/LatoLatin-Regular.eot?#iefix") format("embedded-opentype"),
      url("/static/fonts/Lato/LatoLatin-Regular.woff2") format("woff2"),
      url("/static/fonts/Lato/LatoLatin-Regular.woff") format("woff"),
      url("/static/fonts/Lato/LatoLatin-Regular.ttf") format("truetype");
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
  }

  /* Lato Italic */
  @font-face {
    font-family: "Lato";
    src: url("/static/fonts/Lato/LatoLatin-Italic.eot");
    src: url("/static/fonts/Lato/LatoLatin-Italic.eot?#iefix") format("embedded-opentype"),
      url("/static/fonts/Lato/LatoLatin-Italic.woff2") format("woff2"),
      url("/static/fonts/Lato/LatoLatin-Italic.woff") format("woff"),
      url("/static/fonts/Lato/LatoLatin-Italic.ttf") format("truetype");
    font-style: italic;
    font-weight: normal;
    text-rendering: optimizeLegibility;
  }

  /* Lato Medium 500 */
  @font-face {
    font-family: "Lato";
    src: url("/static/fonts/Lato/LatoLatin-Medium.eot");
    src: url("/static/fonts/Lato/LatoLatin-Medium.eot?#iefix") format("embedded-opentype"),
      url("/static/fonts/Lato/LatoLatin-Medium.woff2") format("woff2"),
      url("/static/fonts/Lato/LatoLatin-Medium.woff") format("woff"),
      url("/static/fonts/Lato/LatoLatin-Medium.ttf") format("truetype");
    font-style: normal;
    font-weight: 500;
    text-rendering: optimizeLegibility;
  }

  /* Lato Medium 500 Italic */
  @font-face {
    font-family: "Lato";
    src: url("/static/fonts/Lato/LatoLatin-MediumItalic.eot");
    src: url("/static/fonts/Lato/LatoLatin-MediumItalic.eot?#iefix") format("embedded-opentype"),
      url("/static/fonts/Lato/LatoLatin-MediumItalic.woff2") format("woff2"),
      url("/static/fonts/Lato/LatoLatin-MediumItalic.woff") format("woff"),
      url("/static/fonts/Lato/LatoLatin-MediumItalic.ttf") format("truetype");
    font-style: italic;
    font-weight: 500;
    text-rendering: optimizeLegibility;
  }

  /* Lato Bold 700 */
  @font-face {
    font-family: "Lato";
    src: url("/static/fonts/Lato/LatoLatin-Bold.eot");
    src: url("/static/fonts/Lato/LatoLatin-Bold.eot?#iefix") format("embedded-opentype"),
      url("/static/fonts/Lato/LatoLatin-Bold.woff2") format("woff2"),
      url("/static/fonts/Lato/LatoLatin-Bold.woff") format("woff"),
      url("/static/fonts/Lato/LatoLatin-Bold.ttf") format("truetype");
    font-style: normal;
    font-weight: 700;
    text-rendering: optimizeLegibility;
  }

  /* Lato Bold 700 Italic */
  @font-face {
    font-family: "Lato";
    src: url("/static/fonts/Lato/LatoLatin-BoldItalic.eot");
    src: url("/static/fonts/Lato/LatoLatin-BoldItalic.eot?#iefix") format("embedded-opentype"),
      url("/static/fonts/Lato/LatoLatin-BoldItalic.woff2") format("woff2"),
      url("/static/fonts/Lato/LatoLatin-BoldItalic.woff") format("woff"),
      url("/static/fonts/Lato/LatoLatin-BoldItalic.ttf") format("truetype");
    font-style: italic;
    font-weight: 700;
    text-rendering: optimizeLegibility;
  }

  /* Lato Heavy 800 */
  @font-face {
    font-family: "Lato";
    src: url("/static/fonts/Lato/LatoLatin-Heavy.eot");
    src: url("/static/fonts/Lato/LatoLatin-Heavy.eot?#iefix") format("embedded-opentype"),
      url("/static/fonts/Lato/LatoLatin-Heavy.woff2") format("woff2"),
      url("/static/fonts/Lato/LatoLatin-Heavy.woff") format("woff"),
      url("/static/fonts/Lato/LatoLatin-Heavy.ttf") format("truetype");
    font-style: normal;
    font-weight: 800;
    text-rendering: optimizeLegibility;
  }

  /* Lato Heavy 800 Italic */
  @font-face {
    font-family: "Lato";
    src: url("/static/fonts/Lato/LatoLatin-HeavyItalic.eot");
    src: url("/static/fonts/Lato/LatoLatin-HeavyItalic.eot?#iefix") format("embedded-opentype"),
      url("/static/fonts/Lato/LatoLatin-HeavyItalic.woff2") format("woff2"),
      url("/static/fonts/Lato/LatoLatin-HeavyItalic.woff") format("woff"),
      url("/static/fonts/Lato/LatoLatin-HeavyItalic.ttf") format("truetype");
    font-style: italic;
    font-weight: 800;
    text-rendering: optimizeLegibility;
  }
`;
