const colors = {
  purple: "#7c22c9",
  purpleLight: "#d8bcef",
  purpleBright: "#9823ff",
  black: "#1b1b1b",
  grey: "#444444",
  greyLight: "#a4a4a4"
};

const space = {
  none: "0",
  xs: "0.25rem",
  sm: "0.5rem",
  md: "1rem",
  lg: "1.5rem",
  xl: "2rem"
};

const breakpoints = ["40em", "52em", "64em"]; // 640, 832, 1024

const mediaQueries = {
  mobile: "@media screen and (max-width: 39.9375em)", //  639 and below
  small: `@media screen and (min-width: ${breakpoints[0]})`,
  medium: `@media screen and (min-width: ${breakpoints[1]})`,
  large: `@media screen and (min-width: ${breakpoints[2]})`
};

const theme = {
  colors,
  space,
  breakpoints,
  mediaQueries
};

export default theme;
