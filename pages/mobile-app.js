import styled from "styled-components";
import { Picture } from "react-responsive-picture";
import { Contain, Flex, Box, Row, Col } from "../components/Grid";
import { theme } from "../components/GlobalStyles";

const Hero = styled.div`
  padding: 2rem 0 0;
  margin-bottom: 2rem;
  background: rgb(92, 40, 208);
  background: linear-gradient(
    180deg,
    rgba(92, 40, 208, 1) 20%,
    rgba(180, 31, 141, 1) 50%,
    rgba(248, 155, 59, 1) 90%
  );
  background-repeat: no-repeat;
  text-align: center;

  .title,
  .subtitle {
    color: #fff;
  }
  .subtitle {
    margin-bottom: 2.8125rem;
  }

  ${theme.mediaQueries.medium} {
    background-size: 100% 80%;
    padding-bottom: 2rem;
    .title {
      font-size: 4.375rem;
    }
    .phones-cluster {
      width: 63.43%;
      max-width: 46.375rem;
    }
  }
`;

const Perk = styled(Col)`
  flex-basis: 50%;
  padding: 1rem;
  img {
    width: 100%;
    max-width: 7.5rem;
    margin-bottom: 1rem;
  }
  ${theme.mediaQueries.medium} {
    flex-basis: 25%;
    img {
      max-width: 12.5rem;
    }
  }
`;

const Feature = styled.div`
  position: relative;
  margin-bottom: 4.5rem;

  &::after {
    position: absolute;
    left: 0;
    top: 150px;
    transform: translateY(-20%);
    z-index: -1;
    height: 350px;
    width: 100%;
    background: url("/static/img/mobile-app/wave1.png") no-repeat center;
    background-size: cover;
    content: "";
  }

  &.wave-2::after {
    background-image: url("/static/img/mobile-app/wave2.png");
    height: 300px;
  }

  &.wave-3::after {
    background-image: url("/static/img/mobile-app/wave3.png");
  }

  ${theme.mediaQueries.medium} {
    &::after {
      height: auto;
      top: 100%;
      transform: translateY(-50%);
      padding-bottom: 34.375%;
    }

    &.wave-2::after {
      background-image: url("/static/img/mobile-app/wave2-wide.png");
      padding-bottom: 37.5%;
    }
    &.wave-3::after {
      top: 130%;
      padding-bottom: 44.270833333%;
    }
    &.no-wave {
      margin-bottom: 100px;
      &::after {
        display: none;
      }
    }
  }

  .feature-img {
    margin: 0 auto 25px;
    position: relative;
    text-align: center;
    img {
      max-width: 215px;
    }

    ${theme.mediaQueries.small} {
      flex: 1 0 33%;
      img {
        max-width: 100%;
      }
    }
  }

  .feature-text {
    ul {
      list-style-position: inside;
      margin-bottom: 2rem;
      padding: 0;
    }
    li {
      font-size: 16px;
    }
  }
`;

const AppLinks = styled.div`
  text-align: center;
  margin-bottom: 20px;
  a {
    margin: 0 0.5rem;
  }
  img {
    height: 50px;
  }
  &.large {
    img {
      height: 62px;
    }
  }
`;

function MobileApp() {
  return (
    <>
      <Hero>
        <Contain>
          <h1 className="title">
            Make Rewards <br />
            even better
          </h1>
          <h2 className="subtitle">Enhance your Plus! Experience with our new app</h2>
          <AppLinks>
            <a href="https://smart.link/5ad95dfcb24b4" rel="noopener noreferrer" target="_blank">
              <img
                className="google-play"
                alt="Google Play Store"
                src="/static/img/mobile-app/google-play.png"
              />
            </a>
            <a href="https://smart.link/5ad95c53044ae" rel="noopener noreferrer" target="_blank">
              <img
                className="app-store"
                alt="App Store"
                src="/static/img/mobile-app/app-store.png"
              />
            </a>
          </AppLinks>
        </Contain>
        <Picture
          className="phones-cluster"
          alt=""
          sources={[
            {
              srcSet: "/static/img/mobile-app/phones-cluster.png",
              media: "(min-width: 768px)"
            },
            {
              srcSet: "/static/img/mobile-app/phones-cluster-mobile.png"
            }
          ]}
        />
      </Hero>
      <Contain css="text-align: center;">
        <h1>Enter a world of delights and surprises</h1>
        <p>
          The Plus! App now brings you even more of the things that you love at your fingertips!
        </p>
        <p>
          Download the app now to discover exclusive discounts, vouchers and exciting rewards from
          your favourite brands such as FairPrice On, ZALORA, Expedia, Mothercare, Texas Chicken and
          many more!
        </p>
        <Row flexWrap="wrap">
          <Perk>
            <img src="/static/img/mobile-app/perk-stamp-card.png" alt="Stamp Cards" />
            <h3>Stamp Cards</h3>
            <p>Earn rewards the fun way</p>
          </Perk>
          <Perk>
            <img src="/static/img/mobile-app/perk-delicious-treats.png" alt="Delicious Treats" />
            <h3>Delicious Treats</h3>
            <p>Savour delicious meals at amazing prices</p>
          </Perk>
          <Perk>
            <img src="/static/img/mobile-app/perk-awesome-deals.png" alt="Awesome deals" />
            <h3>Awesome deals</h3>
            <p>Better way to enjoy your favourite brands</p>
          </Perk>
          <Perk>
            <img src="/static/img/mobile-app/perk-check-lp.png" alt="Check LinkPoints" />
            <h3>Check LinkPoints</h3>
            <p>Keep track of your LinkPoints balance</p>
          </Perk>
        </Row>
      </Contain>

      <Feature>
        <Contain>
          <Flex flexDirection={["column", "row"]} alignItems="center">
            <div className="feature-img">
              <Picture
                alt=""
                sources={[
                  {
                    srcSet: "/static/img/mobile-app/feature-1.png",
                    media: `(min-width: ${theme.breakpoints[1]})`
                  },
                  {
                    srcSet: "/static/img/mobile-app/feature-1-mobile.png"
                  }
                ]}
              />
            </div>
            <Box className="feature-text" ml={["1.25rem", "1.25rem", "2.5rem"]}>
              <h2>Discover deals and offers near you</h2>
              <p>
                Use the Plus! App to find the promotions you like wherever you are, whenever! Be it
                shopping, exercising or travelling, collect the deals you love and retrieve from
                &lsquo;My rewards&rsquo; for when you want to use them.
              </p>
              <ul>
                <li>Pho Street (1-for-1 Monthly Specials)</li>
                <li>Zalora (22% off with no min spend - New to ZALORA only)</li>
                <li>Sephora (Get 10% off at Sephora)</li>
                <li>Changi Rewards (Free 300 points and gift from Changi Rewards!)</li>
              </ul>
            </Box>
          </Flex>
        </Contain>
      </Feature>

      <Feature className="wave-2">
        <Contain>
          <Flex flexDirection={["column", "row-reverse"]} alignItems="center">
            <div className="feature-img">
              <Picture
                alt=""
                sources={[
                  {
                    srcSet: "/static/img/mobile-app/feature-2.png",
                    media: `(min-width: ${theme.breakpoints[1]})`
                  },
                  {
                    srcSet: "/static/img/mobile-app/feature-2-mobile.png"
                  }
                ]}
              />
            </div>
            <Box className="feature-text" mr={["1.25rem", "1.25rem", "2.5rem"]}>
              <h2>Stamp your way to great rewards</h2>
              <p>
                Watch your rewards come to life as you gather stamps each time you visit your
                favourite brands. Complete digital stamp cards on the Plus! App to claim exciting
                rewards like these:
              </p>
              <ul>
                <li>Gong Cha (4 stamps to redeem a free Milk Tea)</li>
                <li>Dunkin’s Donuts (4 stamps and get 1 free regular donut)</li>
                <li>Downtown East (Get 1500 Free LinkPoints)</li>
              </ul>
            </Box>
          </Flex>
        </Contain>
      </Feature>

      <Feature className="wave-3">
        <Contain>
          <Flex flexDirection={["column", "row"]} alignItems="center">
            <div className="feature-img">
              <Picture
                alt=""
                sources={[
                  {
                    srcSet: "/static/img/mobile-app/feature-3.png",
                    media: `(min-width: ${theme.breakpoints[1]})`
                  },
                  {
                    srcSet: "/static/img/mobile-app/feature-3-mobile.png"
                  }
                ]}
              />
            </div>
            <Box className="feature-text" ml={["1.25rem", "1.25rem", "2.5rem"]}>
              <h2>Savour tasty treats you&apos;ll love</h2>
              <p>
                Pamper yourself and loved ones with delicious meals from a curated list of our
                favourite eateries. Explore all food deals under Plus! Treats, and simply present in
                the voucher in store to redeem it!
              </p>
              <ul>
                <li>Four Points Eatery (1-for-1 Sunday Brunch)</li>
                <li>Salad Stop! ($10.00 for Let the Magic in! Wrap - U.P. $13.9)</li>
                <li>Anglo Indian (20% Off Total Bill)</li>
              </ul>
            </Box>
          </Flex>
        </Contain>
      </Feature>

      <Feature className="no-wave">
        <Contain>
          <Flex flexDirection={["column", "row-reverse"]} alignItems="center">
            <div className="feature-img">
              <Picture
                alt=""
                sources={[
                  {
                    srcSet: "/static/img/mobile-app/feature-4.png",
                    media: `(min-width: ${theme.breakpoints[1]})`
                  },
                  {
                    srcSet: "/static/img/mobile-app/feature-4-mobile.png"
                  }
                ]}
              />
            </div>
            <Box className="feature-text" ml={["1.25rem", "1.25rem", "2.5rem"]}>
              <h2>LinkPoints at a glance</h2>
              <p>
                Check your LinkPoints balance and transaction history conveniently! You can also
                manage all of your activity with the Plus! Card on the app.
              </p>
            </Box>
          </Flex>
        </Contain>
      </Feature>

      <Contain className="text-center">
        <h2>Download the Plus! App now</h2>
        <AppLinks className="large">
          <a href="https://smart.link/5ad95dfcb24b4" rel="noopener noreferrer" target="_blank">
            <img
              className="google-play"
              alt="Google Play Store"
              src="/static/img/mobile-app/google-play.png"
            />
          </a>
          <a href="https://smart.link/5ad95c53044ae" rel="noopener noreferrer" target="_blank">
            <img className="app-store" alt="App Store" src="/static/img/mobile-app/app-store.png" />
          </a>
        </AppLinks>
      </Contain>

      <Picture
        alt=""
        sources={[
          {
            srcSet: "/static/img/mobile-app/waves.png",
            media: `(min-width: ${theme.breakpoints[1]})`
          },
          {
            srcSet: "/static/img/mobile-app/waves-mobile.png"
          }
        ]}
      />
    </>
  );
}

export default MobileApp;
