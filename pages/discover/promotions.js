import fetch from "isomorphic-unfetch";
import { Contain, Flex, Box } from "../../components/Grid";
import { parse, format, isBefore, isAfter } from "date-fns";

import Card from "../../components/Cards/Card";
import CardGrid from "../../components/Cards/CardGrid";

function renderPromotions(promotions, ending) {
  switch (promotions.code) {
    case "0000":
      return (
        <>
          <h1>Ending Soon</h1>
          <CardGrid>
            {ending.map(promo => {
              return (
                <Card
                  type="promotion"
                  permalink={promo.permalink}
                  id={promo.id}
                  key={promo.id}
                  name={promo.promotionTitle}
                  logo={promo.merchantLogoUrl}
                  banner={promo.promotionImgUrl}
                  category={promo.categoryName}
                  startDate={promo.startDateFormatted}
                  endDate={promo.endDateFormatted}
                />
              );
            })}
          </CardGrid>
          <h1>Promotions</h1>
          <CardGrid>
            {promotions.response.map(promo => {
              return (
                <Card
                  type="promotion"
                  permalink={promo.permalink}
                  id={promo.id}
                  key={promo.id}
                  name={promo.promotionTitle}
                  logo={promo.merchantLogoUrl}
                  banner={promo.promotionImgUrl}
                  category={promo.categoryName}
                  startDate={promo.startDateFormatted}
                  endDate={promo.endDateFormatted}
                />
              );
            })}
          </CardGrid>
        </>
      );
    case "0001":
      return null;
    case "500":
    default:
      return (
        <>
          <h1>Latest Promotions</h1>
          <div className="error-section">Error retrieving Promotions</div>
        </>
      );
  }
}

function Promotions({ promotions, ending }) {
  return <Contain>{renderPromotions(promotions, ending)}</Contain>;
}

Promotions.getInitialProps = async () => {
  const promotionsData = await fetch(`${process.env.API_URL}/v1/rms`, {
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      body: {
        endPoint: "/promotions",
        method: "Get",
        queryParams: {
          deviceType: 1
        }
      }
    })
  });

  const promotions = await promotionsData.json();

  promotions.response =
    promotions.response &&
    promotions.response.map(promotion => {
      const startDate = parse(promotion.promotionStartDate, "dd/MM/yyyy HH:mm:ss", new Date());
      const endDate = parse(promotion.promotionEndDate, "dd/MM/yyyy HH:mm:ss", new Date());
      const startDateFormatted = format(startDate, "d MMM yyyy");
      const endDateFormatted = format(endDate, "d MMM yyyy");
      return {
        ...promotion,
        startDate,
        endDate,
        startDateFormatted,
        endDateFormatted
      };
    });

  const ending = []
    .concat(promotions.response)
    .sort((a, b) => {
      const first = a.endDate;
      const second = b.endDate;
      if (isBefore(first, second)) return -1;
      if (isAfter(first, second)) return 1;
      return 0;
    })
    .slice(0, 4);

  return { promotions, ending };
};

export default Promotions;
