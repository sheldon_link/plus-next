import OurStoryHeader from "../../components/OurStoryHeader";
import { Contain } from "../../components/Grid";
import Link from "next/link";

export default () => (
  <>
    <OurStoryHeader />
    <Contain>
      <h2>NTUC Link Corporate Profile</h2>
      <p>
        Incorporated in 1998, NTUC Link Private Limited aims to improve lives by stretching the
        dollar on everyday expenses through Plus!, one of Singapore’s largest and most successful
        multi-partners loyalty programme. The Plus! Rewards Programme has more than 2.1 million
        members and over 1,200 participating partner outlets featuring instant redemption, online
        rewards and innovative redemption promotions. Members can also enjoy rewards when using the
        Plus! VISA payment card at millions of VISA locations worldwide.
      </p>
      <p>
        The Plus! Rewards Programme reaffirms NTUC Link’s mission to bring more relevant privileges
        and benefits to Plus! Members, customers of NTUC social enterprises and the general
        population of Singapore.
      </p>
      <h2>Plus! Rewards Programme</h2>
      <p>
        <strong>Earning LinkPoints</strong> - LinkPoints award varies with each Plus! Partner.
        Detailed awarding information is found at each individual partner's page.
      </p>
      <p>
        <strong>Redemption of LinkPoints</strong> - Members can redeem $1 with every 150 LinkPoints.
      </p>
      <img src="/static/img/our-story/infographic.svg" alt="Plus! Rewards Programme Infographic" />
      <p>
        Not a plus! member yet? Sign up now to enjoy the exclusive privileges.{" "}
        <Link href="/apply/cards">
          <a>Join us now {">"}</a>
        </Link>
      </p>
      <h2>Plus! Partners</h2>
      <p>
        The Plus! Rewards Programme has been a significant business enabler to our merchants and
        business partners. Plus! Partners can tap on our membership base and leverage on the
        extensive channels to communicate your promotions. This programme is open to all merchants.
      </p>
      <p>
        Interested to be part of our family? join us as a plus! partner.{" "}
        <a href="/be-our-partner">Read more {">"}</a>
      </p>
    </Contain>
  </>
);
