import OurStoryHeader from "../../components/OurStoryHeader";

export default () => (
  <>
    <OurStoryHeader />
    <div>Facts &amp; Figures</div>
  </>
);
