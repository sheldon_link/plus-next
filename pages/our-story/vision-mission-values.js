import OurStoryHeader from "../../components/OurStoryHeader";

export default () => (
  <>
    <OurStoryHeader />
    <div>Vision, Mission, Values</div>
  </>
);
