import OurStoryHeader from "../../components/OurStoryHeader";

export default () => (
  <>
    <OurStoryHeader />
    <div>Milestones &amp; Achievements</div>
  </>
);
