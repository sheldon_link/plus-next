import { useReducer, useRef, useEffect } from "react";
import Router from "next/router";
import jsCookie from "js-cookie";

import viewResolver from "../components/login/viewResolver";
import { MobileLanding } from "../components/login/views";
import LoginSuccess from "../components/login/views/LoginSuccess";

import Tool from "../components/login/view-tool";

const initialState = {
  view: MobileLanding,
  endpoint: null,
  payload: {},
  additionalHeaders: {},
  viewHistory: [],
  intent: null,
  alert: null,
  tip: null
};

function reducer(state, action) {
  console.log("%c reducing: " + action.type, "color: green");
  switch (action.type) {
    case "UPDATE_PAYLOAD": {
      // update payload
      console.log("<updating payload>");
      console.log(` -- current payload: ${JSON.stringify(state.payload)}`);
      console.log(` -- incoming payload update: ${JSON.stringify(action.value)}`);
      const newPayload = {
        ...state.payload,
        ...action.value
      };
      if (action.remove) {
        delete newPayload[action.remove];
      }
      console.log(` == final payload: ${JSON.stringify(newPayload)}`);
      return {
        ...state,
        payload: newPayload
      };
    }
    case "UPDATE_HEADERS": {
      console.log("<updating headers>");
      console.log(` -- additional headers: ${JSON.stringify(action.value)}`);
      return {
        ...state,
        additionalHeaders: {
          ...state.additionalHeaders,
          ...action.value
        }
      };
    }
    case "CHANGE_VIEW":
      console.log("%c <changing view> - " + action.value.name, "color: blue");
      return {
        ...state,
        tip: null,
        alert: null,
        view: action.value
      };
    case "UPDATE_ENDPOINT":
      return {
        ...state,
        endpoint: action.value
      };
    case "CLEAR_ENDPOINT":
      return {
        ...state,
        endpoint: null
      };
    case "LOGIN_SUCCESS":
      // Router.push("/");
      return {
        ...state,
        view: LoginSuccess
      };
    case "PUSH_VIEW_HISTORY":
      return {
        ...state,
        viewHistory: [].concat([...state.viewHistory, action.value])
      };
    case "BACK": {
      const viewHistory = Array.from(state.viewHistory);
      const nextView = viewHistory.pop();
      return {
        ...state,
        view: nextView,
        viewHistory
      };
    }
    case "UPDATE_ALERT": {
      return {
        ...state,
        alert: {
          message: action.message,
          type: action.messageType
        }
      };
    }
    case "UPDATE_TIP": {
      return {
        ...state,
        tip: action.value
      };
    }
    default:
      return state;
  }
}

function doFetch({ payload, endpoint, additionalHeaders }) {
  return fetch(`${process.env.API_URL}${endpoint}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      ...additionalHeaders
    },
    body: JSON.stringify({
      body: payload,
      metaData: { client: "website" }
    })
  });
}

function handleResult(json, dispatch) {
  if (json.checkToken === 1) {
    dispatch({ type: "UPDATE_HEADERS", value: { Authorization: json.token } });
    jsCookie.set("authToken", json.token, { expires: 7 });
  }
  if (json.code === "0000") {
    const { maskSecurityAnswer } = json.response;
    const { chatMsgCode } = json.response.chatBootresponse;

    // check if logged in
    const loginCodes = ["LOGIN-S1000", "LOGIN-S2000", "LOGIN-S3000", "LOGIN-S4000"];
    if (loginCodes.indexOf(chatMsgCode) > -1) {
      console.log("login success");
      dispatch({ type: "LOGIN_SUCCESS" });
    } else {
      // continue onboarding
      if (maskSecurityAnswer) {
        dispatch({ type: "UPDATE_TIP", value: maskSecurityAnswer });
      }
      const nextView = viewResolver(json);
      dispatch({ type: "CHANGE_VIEW", value: nextView });
    }
  } else {
    // error
    console.error(json.message);
    if (json.message) {
      dispatch({ type: "UPDATE_ALERT", message: json.message, messageType: "error" });
    }
  }
  dispatch({ type: "CLEAR_ENDPOINT" });
}

function Login() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const isFirst = useRef(true);

  useEffect(() => {
    if (isFirst.current) {
      isFirst.current = false;
      return;
    }
    console.log(`useEffect (endpoint changed): ${state.endpoint}`);
    console.log(state.payload);

    if (!state.endpoint) return;

    console.log("calling endpoint");
    const fetcher = async function fetcher() {
      const res = await doFetch(state);
      const json = await res.json();
      console.log(json);
      handleResult(json, dispatch);
    };
    fetcher();
  }, [state.endpoint]);

  const CurrentView = state.view;

  // return <CurrentView state={state} dispatch={dispatch} />;
  return (
    <>
      <Tool state={state} dispatch={dispatch} />
      <CurrentView state={state} dispatch={dispatch} />
    </>
  );
}

export default Login;
