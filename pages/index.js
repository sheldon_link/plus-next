import React from "react";
import fetch from "isomorphic-unfetch";
import Link from "next/link";
import styled, { css } from "styled-components";

import StyledSlider from "../components/Home/StyledSlider";
import Hero from "../components/Home/Hero";
import { Flex, Box } from "../components/Grid";

const FittedImage = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

function Home({ errorCode, data }) {
  if (errorCode) {
    console.error("Error: ", errorCode);
    return <div>error {errorCode}</div>;
  }
  const sliderArray = (data.response && data.response.sliders) || [];
  const adArray = (data.response && data.response.advertisements) || [];
  const featuredPartnersArray = (data.response && data.response.featuredPartners) || [];
  const featuredPromotionsArray = (data.response && data.response.featuredPromotions) || [];
  return (
    <>
      <Hero className="contain">
        <ul className="quick-links">
          <li>
            <Link href="/apply/cards">
              <a>
                Apply for a Plus! card
                <div className="quick-links-text">If you don't already have one</div>
              </a>
            </Link>
          </li>
          <li>
            <Link href="/mobile-app">
              <a>
                Download Plus! app
                <div className="quick-links-text">Sign up now for a FREE* Plus! card</div>
              </a>
            </Link>
          </li>
          <li>
            <Link href="/registration">
              <a>
                Register your card
                <div className="quick-links-text">For Plus! cards purchased in-store</div>
              </a>
            </Link>
          </li>
          <li>
            <Link href="/profile/card-replacement">
              <a>
                Card replacement
                <div className="quick-links-text">For lost or damaged Plus! card</div>
              </a>
            </Link>
          </li>
        </ul>
        <div className="carousel">
          <StyledSlider fade dots infinite speed={500} slidesToShow={1} slidesToSscroll={1}>
            {sliderArray.map(item => (
              <div key={item.id}>
                <a href={item.pageUrl}>
                  <img alt={item.title} src={item.imageUrl} />
                </a>
              </div>
            ))}
          </StyledSlider>
        </div>
      </Hero>

      <div className="contain">
        <h2 className="text-center">Plus! Partners</h2>
        <Flex flexWrap="wrap" justifyContent="center" mb="lg">
          {featuredPartnersArray.map(partner => (
            <a
              css={css`
                margin: 0.5% 4%;
              `}
              key={partner.id}
              href={`merchant/${partner.permalink}`}
            >
              <img
                css={css`
                  max-width: 96px;
                `}
                src={partner.merchantLogoUrl}
                alt={partner.title}
              />
            </a>
          ))}
        </Flex>
      </div>

      <Flex flexDirection={["column", "row"]} className="contain" mb="1rem">
        {adArray.map(ad => (
          <Box mx={["none", "sm"]} my={["sm", "none"]} className="advertisement" key={ad.id}>
            <a href={ad.pageUrl} target="_blank">
              <FittedImage alt="" src={ad.imageUrl} />
            </a>
          </Box>
        ))}
      </Flex>

      <div className="contain">
        <h2 className="text-center">Promotions</h2>
        <Flex flexDirection={["column", "row"]} mb="5rem">
          {featuredPromotionsArray.map((promo, index) => (
            <Box mx={["none", "sm"]} className="promo" key={index}>
              <a href={`/discover/promotion/${promo.permalink}`}>
                <FittedImage alt={promo.title} src={promo.headerImgUrl} />
              </a>
            </Box>
          ))}
        </Flex>
      </div>
    </>
  );
}

Home.getInitialProps = async () => {
  const res = await fetch(`${process.env.API_URL}/v1/rms`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      body: {
        accessId: "",
        endPoint: "/homepage",
        method: "GET",
        queryParams: {
          featureMerchantCategory: "Featured Merchants (Web Homepage)",
          featurePromotionCategory: "Featured Promotions (Web Homepage)"
        }
      }
    })
  });
  const errorCode = res.status > 200 ? res.status : false;
  const data = errorCode ? null : await res.json();
  return { errorCode, data };
};

export default Home;
