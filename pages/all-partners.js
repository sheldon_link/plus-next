import fetch from "isomorphic-unfetch";
import styled from "styled-components";
import { parse, format } from "date-fns";

import Card from "../components/Cards/Card";
import CardGrid from "../components/Cards/CardGrid";
import CardsList from "../components/Cards/CardList";
import { Contain } from "../components/Grid";

const FeaturedRail = styled.div`
  position: relative;
  overflow: hidden;
  margin-bottom: 2rem;

  &::before,
  &::after {
    content: "";
    background-image: linear-gradient(to right, #fff, rgba(255, 255, 255, 0));
    position: absolute;
    left: 0;
    height: 100%;
    width: 15px;
    z-index: 2;
  }

  &::after {
    background-image: linear-gradient(to left, #fff, rgba(255, 255, 255, 0));
    left: auto;
    top: 0;
    right: 0;
  }
`;

function renderFeatured(featured) {
  switch (featured.code) {
    case "0000":
      return (
        <>
          <h1>Featured Partners</h1>
          <FeaturedRail>
            <CardGrid rail>
              {featured.response.map(partner => {
                return (
                  <Card
                    key={partner.id}
                    permalink={partner.permalink}
                    name={partner.merchantName}
                    logo={partner.merchantBigLogoURL}
                    banner={partner.headerURL}
                  />
                );
              })}
            </CardGrid>
          </FeaturedRail>
        </>
      );

    case "500":
    default:
      return <div className="error-section">Error retrieving Featured Partners</div>;
  }
}

function renderAllPartners(allPartners) {
  switch (allPartners.code) {
    case "0000":
      return (
        <>
          <h1>All Partners</h1>
          <CardsList
            cards={allPartners.response.partnerList}
            filterOptions={allPartners.response.filter}
          />
        </>
      );
    case "500":
    default:
      return <div className="error-section">Error retrieving partners</div>;
  }
}

function renderPromotions(promotions) {
  switch (promotions.code) {
    case "0000":
      return (
        <>
          <h1>Latest Promotions</h1>
          <CardGrid>
            {promotions.response.slice(0, 4).map(promo => {
              const startDate = format(
                parse(promo.promotionStartDate, "dd/MM/yyyy HH:mm:ss", new Date()),
                "d MMM yyyy"
              );
              const endDate = format(
                parse(promo.promotionEndDate, "dd/MM/yyyy HH:mm:ss", new Date()),
                "d MMM yyyy"
              );
              return (
                <Card
                  key={promo.id}
                  type="promotion"
                  permalink={promo.permalink}
                  name={promo.promotionTitle}
                  logo={promo.merchantLogoUrl}
                  banner={promo.promotionImgUrl}
                  category={promo.categoryName}
                  startDate={startDate}
                  endDate={endDate}
                />
              );
            })}
          </CardGrid>
        </>
      );
    case "0001":
      return null;
    case "500":
    default:
      return (
        <>
          <h1>Latest Promotions</h1>
          <div className="error-section">Error retrieving Promotions</div>
        </>
      );
  }
}

function AllPartners({ featured, allPartners, promotions }) {
  return (
    <Contain>
      {renderFeatured(featured)}
      {renderAllPartners(allPartners)}
      {renderPromotions(promotions)}
    </Contain>
  );
}

AllPartners.getInitialProps = async () => {
  // featured
  const fetchFeatured = fetch(`${process.env.API_URL}/v1/rms`, {
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      body: {
        endPoint: "/v2/merchants",
        method: "get",
        queryParams: {
          "category-name": "Featured Merchants (Web)"
        }
      }
    })
  });

  // all partners
  const fetchAllPartners = fetch(`${process.env.API_URL}/v1/rms`, {
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      body: {
        endPoint: "/v2/partners",
        method: "Get"
      }
    })
  });

  // promotions
  const fetchPromotions = fetch(`${process.env.API_URL}/v1/rms`, {
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      body: {
        endPoint: "/promotions",
        method: "Get",
        queryParams: {
          deviceType: 1
        }
      }
    })
  });

  const [featuredData, allPartnersData, promotionsData] = await Promise.all([
    fetchFeatured,
    fetchAllPartners,
    fetchPromotions
  ]);
  const [featured, allPartners, promotions] = await Promise.all([
    featuredData.json(),
    allPartnersData.json(),
    promotionsData.json()
  ]);

  return { featured, allPartners, promotions };
};

export default AllPartners;
