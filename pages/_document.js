import Document, { Html, Head, Main, NextScript } from "next/document";
import { ServerStyleSheet } from "styled-components";

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: App => props => sheet.collectStyles(<App {...props} />)
        });

      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        )
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    return (
      <Html lang="en">
        <Head>
          <script
            crossOrigin="anonymous"
            src="https://polyfill.io/v3/polyfill.min.js?features=Array.from%2CArray.of%2CArray.prototype.fill%2CArray.prototype.find%2CArray.prototype.findIndex%2CEvent%2CCustomEvent%2CDOMTokenList%2CElement.prototype.after%2CElement.prototype.append%2CElement.prototype.before%2CElement.prototype.matches%2CElement.prototype.closest%2CElement.prototype.prepend%2CElement.prototype.remove%2CElement.prototype.replaceWith%2CSymbol%2CSymbol.iterator%2CSymbol.species%2CNumber.isNaN%2CMap%2CNode.prototype.contains%2CObject.assign%2CPromise%2CSet%2CString.prototype.endsWith%2CString.prototype.includes%2CString.prototype.startsWith%2CURL%2CArray.prototype.includes%2CObject.values"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
