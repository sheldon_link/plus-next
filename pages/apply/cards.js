import { useState, useReducer, memo } from "react";
import { css } from "styled-components";
import { CSSTransition } from "react-transition-group";

import lineBreaker from "../../components/utils/lineBreaker";
import Button from "../../components/Button";
import { Contain, Flex, Box, Row, Col } from "../../components/Grid";
import {
  OuterRail,
  Rail,
  Card,
  CardTitle,
  CaretButton,
  TextHolder,
  Fader,
  Option,
  Table
} from "../../components/apply/ApplyUI";

const CompareTableMemo = memo(CompareTable);

const cardsReducer = (state, action) => {
  const activeIndex = state.cards.findIndex(card => card.active);
  const newState = { ...state, inProp: false };
  let nextIndex;
  switch (action.type) {
    case "PREV":
      if (activeIndex === 0) return state;
      nextIndex = activeIndex - 1;
      break;
    case "NEXT":
      if (activeIndex === newState.cards.length - 1) return state;
      nextIndex = activeIndex + 1;
      break;
    case "GOTO_CARD":
      nextIndex = action.value;
      break;
    case "FADE_IN":
      return { ...state, inProp: true };
    default:
      return state;
  }
  if (activeIndex === nextIndex) return state;
  newState.cards[activeIndex].active = false;
  newState.cards[nextIndex].active = true;
  return newState;
};

function CardsPage({ data }) {
  const initialState = {
    cards: data,
    inProp: true
  };
  const [state, dispatch] = useReducer(cardsReducer, initialState);
  const theActiveCard = state.cards.find(card => card.active);
  const theActiveOffset = state.cards.indexOf(theActiveCard);
  const [content, setContent] = useState(theActiveCard);

  const updateContent = () => {
    setContent(theActiveCard);
    dispatch({ type: "FADE_IN" });
  };

  function handleDragStart(e) {
    const event = e.touches[0];
    const startX = event.clientX;
    e.currentTarget.addEventListener("touchend", handleDragEnd);

    function handleDragEnd(evt) {
      const endX = evt.changedTouches[0].clientX;
      const delta = endX - startX;
      if (delta < -20) {
        dispatch({ type: "NEXT" });
      } else if (delta > 20) {
        dispatch({ type: "PREV" });
      }
      evt.currentTarget.removeEventListener("touchend", handleDragEnd);
    }
  }

  return (
    <div
      css={`
        overflow: hidden;
      `}
    >
      <Contain className="text-center">
        <h1>Get a Plus! card</h1>
        <p>
          Join 2 million Plus! members and swipe your way to a world of benefits and privileges!
        </p>
      </Contain>
      <OuterRail>
        <Rail offset={theActiveOffset} onTouchStart={handleDragStart}>
          {state.cards.map((item, i) => (
            <Card
              key={i}
              active={item.active}
              onClick={() => !item.active && dispatch({ type: "GOTO_CARD", value: i })}
            >
              <img alt="" src={item.imgUrl} />
            </Card>
          ))}
        </Rail>
      </OuterRail>
      <Contain>
        <Flex mt={[0, "2rem"]} flexDirection="column">
          <Flex
            maxWidth="35rem"
            width="100%"
            mx="auto"
            my="sm"
            flex="1"
            alignItems="center"
            onTouchStart={handleDragStart}
          >
            <CaretButton
              disabled={theActiveOffset <= 0}
              type="button"
              className="left"
              onClick={() => dispatch({ type: "PREV" })}
            />
            <Box flex="1">
              <CSSTransition in={state.inProp} classNames="fade" timeout={150}>
                <Fader>
                  <CardTitle>{content.name}</CardTitle>
                </Fader>
              </CSSTransition>
            </Box>
            <CaretButton
              type="button"
              disabled={theActiveOffset >= state.cards.length - 1}
              className="right"
              onClick={() => dispatch({ type: "NEXT" })}
            />
          </Flex>
          <TextHolder>
            <CSSTransition
              in={state.inProp}
              timeout={150}
              classNames="fade"
              onExited={updateContent}
            >
              <Fader>{renderContent(content)}</Fader>
            </CSSTransition>
          </TextHolder>
        </Flex>
        <Flex mb="md" justifyContent="center">
          <Box>
            <h2>Compare other cards</h2>
          </Box>
        </Flex>
        <CompareTableMemo />
      </Contain>
    </div>
  );
}

CardsPage.getInitialProps = async () => {
  return {
    data: [
      {
        name: "Plus! Visa Credit/Debit Card",
        features: [
          "Up to 7% rebate at FairPrice",
          "Instant 18% off at Caltex, and up to 18.5% off at Esso",
          "Up to 50% off with OCBC merchants"
        ],
        imgUrl: "/static/img/common/cards/plus-visa-credit-card@2x.png",
        options: [
          {
            title: "Apply for Credit Card",
            description: "Enjoy a credit limit of up to 4 times monthly income.",
            buttonTitle: "Apply now",
            buttonUrl: "http://google.com"
          },
          {
            title: "Apply for Debit Card",
            description: "Save more with an interest rate of up to 0.15% per year.",
            buttonTitle: "Apply now",
            buttonUrl: "http://yahoo.com"
          }
        ],
        active: false
      },
      {
        name: "NTUC Plus! card",
        imgUrl: "/static/img/common/cards/ntuc-plus-card@2x.png",
        features: [
          "Union membership benefits & privileges",
          "Over $2,000 savings on daily expenses",
          "Up to 4% cash rebate at FairPrice annually",
          "Extended LinkPoints validity period"
        ],
        options: [
          {
            title: "Apply online",
            description:
              "You will be redirected to NTUC Membership’s website for more details on getting this card.",
            buttonTitle: "Proceed",
            buttonUrl: "http://bing.com"
          }
        ],
        active: false
      },
      {
        name: "Plus! card",
        imgUrl: "/static/img/common/cards/black-card@2x.png",
        features: [
          "Earn LinkPoints at over 1,000 partner locations",
          "Use LinkPoints to offset your daily expenses",
          "Save up to 50% with LinkPoints on rewards",
          "No minimum age restrictions",
          "No annual fees"
        ],
        options: [
          {
            title: "Apply online",
            description: "A one-time $10 fee applies. \nYou'll get a 150 LinkPoints reward!",
            buttonTitle: "Apply now",
            buttonUrl: "http://google.com",
            width: "33%"
          },
          {
            title: "Apply via the Plus! app",
            description: "Download and register via the mobile app for free!",
            buttonTitle: "Download now",
            buttonUrl: "http://yahoo.com",
            width: "66%"
          }
        ],
        active: true
      },
      {
        name: "NTUC Plus! Visa debit/credit card",
        imgUrl: "/static/img/common/cards/ntuc-plus-visa-credit-card@2x.png",
        features: [
          "Union membership benefits & privileges",
          "Up to 12% rebate at FairPrice",
          "Instant 18% off at Caltex, and up to 18.5% off at Esso",
          "Up to 50% off with OCBC merchants",
          "Extended LinkPoints validity period"
        ],
        options: [
          {
            title: "Apply for credit",
            description: "Enjoy a credit limit of up to 4 times monthly income.",
            buttonTitle: "Apply now",
            buttonUrl: "http://google.com"
          },
          {
            title: "Apply for debit",
            description: "Save more with an interest rate of up to 0.15% per year.",
            buttonTitle: "Apply now",
            buttonUrl: "http://yahoo.com"
          }
        ],
        active: false
      },
      {
        name: "nEbO card",
        imgUrl: "/static/img/common/cards/nebo-card@2x.png",
        features: [
          "Enjoy exclusive lifestyle & entertainment privileges for youth",
          "Earn & redeem LinkPoints at over 1,000 partner locations",
          "For youth & non-working individuals aged 12 – 25"
        ],
        options: [
          {
            title: "Apply online",
            description:
              "You will be redirected to NTUC Membership’s website for more details on getting this card.",
            buttonTitle: "Proceed",
            buttonUrl: "http://google.com"
          }
        ],
        active: false
      }
    ]
  };
};

function renderContent({ features, options }) {
  return (
    <>
      <ul>
        {features.map((feature, i) => (
          <li key={i}>{feature}</li>
        ))}
      </ul>
      <Row flexDirection={["column", "row"]}>
        {options.map((option, i) => (
          <Col flex="1" flexBasis={option.width || null} mb="md" key={i}>
            <Option>
              <h2>{option.title}</h2>
              <p>{lineBreaker(option.description)}</p>
              <Button mt="auto" alignSelf={["stretch", "auto"]} href={option.buttonUrl}>
                {option.buttonTitle}
              </Button>
            </Option>
          </Col>
        ))}
      </Row>
    </>
  );
}

function CompareTable() {
  return (
    <div
      css={css`
        overflow-x: auto;
        margin-bottom: 2rem;
      `}
    >
      <Table>
        <thead>
          <tr className="card-images">
            <th />
            <th>
              <img src="/static/img/common/cards/black-card@2x.png" />
            </th>
            <th>
              <img src="/static/img/common/cards/ntuc-plus-card@2x.png" />
            </th>
            <th>
              <img src="/static/img/common/cards/plus-visa-credit-card@2x.png" />
            </th>
            <th>
              <img src="/static/img/common/cards/ntuc-plus-visa-credit-card@2x.png" />
            </th>
            <th>
              <img src="/static/img/common/cards/nebo-card@2x.png" />
            </th>
          </tr>
          <tr className="card-titles">
            <th />
            <th>Plus! card</th>
            <th>NTUC Plus! card</th>
            <th>Plus! Visa debit/credit card</th>
            <th>NTUC Plus! Visa debit/credit card</th>
            <th>nEbO card</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Lifetime membership*</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
          </tr>
          <tr>
            <td>Earn & redeem LinkPoints with Plus! partners</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
          </tr>
          <tr>
            <td>Accelerated LinkPoints earn with selected Plus! partners</td>
            <td className="symbol">×</td>
            <td className="symbol">✓</td>
            <td className="symbol">×</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
          </tr>
          <tr>
            <td>Earn LinkPoints on Plus! Visa transactions*</td>
            <td className="symbol">×</td>
            <td className="symbol">×</td>
            <td>0.22%</td>
            <td>0.22%</td>
            <td className="symbol">×</td>
          </tr>
          <tr>
            <td>Enjoy savings at FairPrice and Unity Pharmacy*</td>
            <td>1.33%</td>
            <td>5.3%</td>
            <td>7%</td>
            <td>Up to 12%</td>
            <td className="symbol">×</td>
          </tr>
          <tr>
            <td>Fuel savings at Caltex*</td>
            <td className="symbol">×</td>
            <td className="symbol">×</td>
            <td>At least 18.2%</td>
            <td>At least 18.2%</td>
            <td className="symbol">×</td>
          </tr>
          <tr>
            <td>Fuel savings at Esso*</td>
            <td className="symbol">×</td>
            <td className="symbol">×</td>
            <td>Up to 18.5%</td>
            <td>Up to 18.5%</td>
            <td className="symbol">×</td>
          </tr>
          <tr>
            <td>Enjoy savings at POPULAR</td>
            <td className="symbol">×</td>
            <td className="symbol">×</td>
            <td>3%</td>
            <td>3%</td>
            <td className="symbol">×</td>
          </tr>
          <tr>
            <td>Flash discounts at participating NTUC merchants</td>
            <td className="symbol">×</td>
            <td className="symbol">✓</td>
            <td className="symbol">×</td>
            <td className="symbol">✓</td>
            <td className="symbol">✓</td>
          </tr>
          <tr>
            <td>Enjoy savings at participating OCBC merchants</td>
            <td className="symbol">×</td>
            <td className="symbol">×</td>
            <td>Up to 50%</td>
            <td>Up to 50%</td>
            <td className="symbol">×</td>
          </tr>
          <tr>
            <td>Career & community support</td>
            <td className="symbol">×</td>
            <td className="symbol">✓</td>
            <td className="symbol">×</td>
            <td className="symbol">✓</td>
            <td className="symbol">×</td>
          </tr>
          <tr>
            <td>*Other terms & conditions apply.</td>
            <td>
              <Button>Get this</Button>
            </td>
            <td>
              <Button>Get this</Button>
            </td>
            <td>
              <Button>Get this</Button>
            </td>
            <td>
              <Button>Get this</Button>
            </td>
            <td>
              <Button>Get this</Button>
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

export default CardsPage;
